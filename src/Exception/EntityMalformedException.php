<?php declare(strict_types = 1);

namespace Roensby\SymfonyDrupalJsonApi\Exception;

use Roensby\SymfonyDrupalJsonApi\Exception\Base\DrupalJsonApiException;

class EntityMalformedException extends DrupalJsonApiException
{
}
