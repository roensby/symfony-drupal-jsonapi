<?php declare(strict_types = 1);

namespace Roensby\SymfonyDrupalJsonApi\Exception;

class EntityNotFoundException extends EntityException
{
}
