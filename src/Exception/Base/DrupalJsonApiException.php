<?php declare(strict_types = 1);

namespace Roensby\SymfonyDrupalJsonApi\Exception\Base;

use Exception;

abstract class DrupalJsonApiException extends Exception implements DrupalJsonApiExceptionInterface
{
}
