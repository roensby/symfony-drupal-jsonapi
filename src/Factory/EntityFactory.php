<?php declare(strict_types = 1);

namespace Roensby\SymfonyDrupalJsonApi\Factory;

use Roensby\SymfonyDrupalJsonApi\JsonApi\Query;
use Roensby\SymfonyDrupalJsonApi\JsonApi\QueryInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class EntityFactory
{
    /**
     * @var HttpClientInterface
     */
    protected $client;

    /**
     * @var string
     */
    protected $endpoint;

    /**
     * @var RequestStack
     */
    protected $requestStack;

    /**
     * @var Security
     */
    protected $security;

    public function __construct(array $configuration, HttpClientInterface $client, RequestStack $requestStack, Security $security)
    {
        $this->client = $client;
        $this->endpoint = $configuration['endpoint'];
        $this->requestStack = $requestStack;
        $this->security = $security;
    }

    public function query(string $entityClass): QueryInterface
    {
        return new Query($entityClass, $this->client, $this->requestStack, $this->security, $this->endpoint);
    }
}
