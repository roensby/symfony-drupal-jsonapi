<?php declare(strict_types = 1);

namespace Roensby\SymfonyDrupalJsonApi;

class Constant
{
    /**
     * JSON:API constants.
     */
    const DATETIME_FORMAT = 'Y-m-d\TH:i:s';
    const DATE_FORMAT = 'Y-m-d';
    // For performance reasons, the Drupal jsonapi module limits number of
    // results to 50. See https://www.drupal.org/project/jsonapi/issues/2793233.
    // Note: this includes results that the user doesn't have access to.
    const JSONAPI_LIMIT = 50;
    const JSONAPI_REQUEST_METHOD_DELETE = 'DELETE';
    const JSONAPI_REQUEST_METHOD_GET = 'GET';
    const JSONAPI_REQUEST_METHOD_PATCH = 'PATCH';
    const JSONAPI_REQUEST_METHOD_POST = 'POST';
    const JSONAPI_HEADER_CONTENT_TYPE = 'application/vnd.api+json';
    const JSONAPI_HEADER_CONTE_TYPE_FILE = 'application/octet-stream';
    const JSONAPI_HEADER_ACCEPT = 'application/vnd.api+json';
    const JSONAPI_ISO_8601_DATETIME_FORMAT = 'Y-m-d\TH:i:sP';

    // See https://symfony.com/doc/current/security.html#roles.
    const SYMFONY_ROLE_PREFIX = 'ROLE_';

    /**
     * Drupal entities, bundles, field names and vocabularies.
     */
    const ENTITY_COMMENT = 'comment';
    const ENTITY_FILE = 'file';
    const ENTITY_MEDIA = 'media';
    const ENTITY_NODE = 'node';
    const ENTITY_TERM = 'taxonomy_term';
    const ENTITY_USER = 'user';
    const ENTITY_USER_ROLE = 'user_role';
    const ENTITY_VOCABULARY = 'taxonomy_vocabulary';
    const BUNDLE_AUDIO = 'audio';
    const BUNDLE_FILE = 'file';
    const BUNDLE_IMAGE = 'image';
    const BUNDLE_TAGS = 'tags';
    const BUNDLE_VIDEO = 'video';
    const FIELD_ARTICLE_IMAGE = 'field_image';
    const FIELD_MEDIA_AUDIO = 'audio_file';
    const FIELD_MEDIA_FILE = 'file';
    const FIELD_MEDIA_IMAGE = 'image_file';
    const FIELD_MEDIA_VIDEO = 'video_file';
    const UNDEFINED = null;
}
