<?php declare(strict_types = 1);

namespace Roensby\SymfonyDrupalJsonApi\DependencyInjection;

use Roensby\SymfonyDrupalJsonApi\Factory\EntityFactory;
use Roensby\SymfonyDrupalJsonApi\Security\GuardAuthenticator\LoginAuthenticator;
use Roensby\SymfonyDrupalJsonApi\Security\GuardAuthenticator\OAuthRefreshAuthenticator;
use Roensby\SymfonyDrupalJsonApi\Security\GuardAuthenticator\PresetAuthenticator;
use Roensby\SymfonyDrupalJsonApi\Storage\EntityStorage;
use Roensby\SymfonyDrupalJsonApi\Storage\FileStorage;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class SymfonyDrupalJsonapiExtension extends Extension
{
    /**
     * @throws \Exception
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
        $definition = $container->getDefinition(EntityFactory::class);
        $definition->addArgument($config['jsonapi']);
        $definition = $container->getDefinition(EntityStorage::class);
        $definition->addArgument($config['jsonapi']);
        $definition = $container->getDefinition(FileStorage::class);
        $definition->addArgument($config['jsonapi']);
        $definition = $container->getDefinition(LoginAuthenticator::class);
        $definition->addArgument($config['oauth2']);
        $definition = $container->getDefinition(PresetAuthenticator::class);
        $definition->addArgument($config['oauth2']);
        $definition = $container->getDefinition(OAuthRefreshAuthenticator::class);
        $definition->addArgument($config['oauth2']);
    }

    public function getAlias()
    {
        return 'symfony_drupal_jsonapi';
    }
}
