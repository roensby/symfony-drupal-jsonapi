<?php declare(strict_types = 1);

namespace Roensby\SymfonyDrupalJsonApi\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('symfony_drupal_jsonapi');
        $treeBuilder->getRootNode()
            ->children()
                ->arrayNode('jsonapi')
                    ->children()
                        ->scalarNode('endpoint')->defaultValue('http://example/jsonapi')->isRequired()->end()
                        ->scalarNode('csrf_endpoint')->defaultValue('http://example/session/token')->isRequired()->end()
                    ->end()
                ->end() // jsonapi
                ->arrayNode('oauth2')
                    ->children()
                        ->scalarNode('endpoint')->defaultValue('http://example/oauth/token')->isRequired()->end()
                        ->scalarNode('client_id')->defaultValue('%env(string:SYMFONY_DRUPAL_JSONAPI_CLIENT_ID)%')->isRequired()->end()
                        ->scalarNode('client_secret')->defaultValue('%env(string:SYMFONY_DRUPAL_JSONAPI_CLIENT_SECRET)%')->isRequired()->end()
                        ->arrayNode('interactive_login')
                            ->children()
                                ->scalarNode('login_route')->defaultValue('app_login')->end()
                                ->scalarNode('login_redirect')->defaultValue('frontpage')->end()
                            ->end()
                        ->end() // interactive_login
                        ->arrayNode('preset_user')
                            ->children()
                                ->scalarNode('username')->defaultValue('%env(string:SYMFONY_DRUPAL_JSONAPI_PRESET_USER_USERNAME)%')->end()
                                ->scalarNode('password')->defaultValue('%env(string:SYMFONY_DRUPAL_JSONAPI_PRESET_USER_PASSWORD)%')->end()
                            ->end()
                        ->end() // preset_user
                    ->end()
                ->end() // oauth2
            ->end()
        ;
        return $treeBuilder;
    }
}
