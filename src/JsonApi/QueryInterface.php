<?php

namespace Roensby\SymfonyDrupalJsonApi\JsonApi;

use Roensby\SymfonyDrupalJsonApi\Entity\Base\EntityBaseInterface;

interface QueryInterface
{
    public function addFilter(Filter $filter): QueryInterface;

    public function addIncludePath(IncludePath $path): QueryInterface;

    public function addSortOrder(SortOrder $sortOrder): QueryInterface;

    public function getCount(): ?int;

    /**
     * @throws \Roensby\SymfonyDrupalJsonApi\Exception\EntityException
     * @throws \Roensby\SymfonyDrupalJsonApi\Exception\EntityNotFoundException
     */
    public function load(): ?EntityBaseInterface;

    /**
     * @throws \Roensby\SymfonyDrupalJsonApi\Exception\EntityException
     * @throws \Roensby\SymfonyDrupalJsonApi\Exception\EntityNotFoundException
     */
    public function loadMultiple(): array;

    public function setLimit(int $limit): QueryInterface;

    public function setOffset(int $offset): QueryInterface;

    public function setResourceVersion(?ResourceVersionInterface $resourceVersion): QueryInterface;

    public function setUuid(string $uuid): QueryInterface;
}
