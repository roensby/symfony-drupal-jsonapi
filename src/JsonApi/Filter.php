<?php declare(strict_types = 1);

namespace Roensby\SymfonyDrupalJsonApi\JsonApi;

use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Mapping\ClassMetadata;

class Filter implements FilterInterface
{
    /**
     * @var ?ConditionGroup
     */
    protected $memberOf;

    /**
     * @var ?string
     */
    protected $operator = '=';

    /**
     * @var ?string
     */
    protected $path;

    /**
     * @var ?string
     */
    protected $value;

    public function __construct(string $path = null, string $operator = null, string $value = null, ConditionGroupInterface $conditionGroup = null)
    {
        $this->memberOf = $conditionGroup;
        $this->operator = $operator;
        $this->path = $path;
        $this->value = $value;
    }

    /**
     * Getters.
     */

    public function getMemberOf(): ?ConditionGroupInterface
    {
        return $this->memberOf;
    }

    public function getOperator(): string
    {
        return $this->operator;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * Validation.
     *
     * @param ClassMetadata $metadata
     */
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('operator', new NotBlank());
        $metadata->addPropertyConstraint('path', new NotBlank());
    }

    /**
     * Setters.
     */

    public function setMemberOf(?ConditionGroupInterface $conditionGroup): FilterInterface
    {
        $this->memberOf = $conditionGroup;
        return $this;
    }

    public function setOperator(string $operator): FilterInterface
    {
        $this->operator = $operator;
        return $this;
    }

    public function setPath(string $path): FilterInterface
    {
        $this->path = $path;
        return $this;
    }

    public function setValue(string $value): FilterInterface
    {
        $this->value = $value;
        return $this;
    }
}
