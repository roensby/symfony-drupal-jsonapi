<?php

namespace Roensby\SymfonyDrupalJsonApi\JsonApi;

interface SortOrderInterface
{
    /**
     * Getters.
     */

    public function getDirection(): string;

    public function getPath(): string;

    /**
     * Setters.
     */

    public function setDirection(string $direction): SortOrderInterface;

    public function setPath(string $path): SortOrderInterface;
}
