<?php declare(strict_types = 1);

namespace Roensby\SymfonyDrupalJsonApi\JsonApi;

use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Mapping\ClassMetadata;

class IncludePath implements IncludePathInterface
{
    /**
     * @var string
     */
    protected $path;

    public function __construct(string $path)
    {
        $this->path = $path;
    }

    /**
     * Getters.
     */

    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * Validation.
     *
     * @param ClassMetadata $metadata
     */
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('path', new NotBlank());
    }

    /**
     * Setters.
     */

    public function setPath(string $path): IncludePathInterface
    {
        $this->path = $path;
        return $this;
    }
}
