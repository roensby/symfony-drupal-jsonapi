<?php declare(strict_types = 1);

namespace Roensby\SymfonyDrupalJsonApi\JsonApi;

use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Mapping\ClassMetadata;

class SortOrder implements SortOrderInterface
{
    /**
     * @var string
     */
    protected $direction = 'ASC';

    /**
     * @var string
     */
    protected $path;

    public function __construct(string $path = null, string $direction = null)
    {
        $this->direction = $direction;
        $this->path = $path;
    }

    /**
     * Getters.
     */

    public function getDirection(): string
    {
        return $this->direction;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * Validation.
     *
     * @param ClassMetadata $metadata
     */
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('direction', new NotBlank());
        $metadata->addPropertyConstraint('path', new NotBlank());
    }

    /**
     * Setters.
     */

    public function setDirection(string $direction): SortOrderInterface
    {
        $this->direction = $direction;
        return $this;
    }

    public function setPath(string $path): SortOrderInterface
    {
        $this->path = $path;
        return $this;
    }
}
