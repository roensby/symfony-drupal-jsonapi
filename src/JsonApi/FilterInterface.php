<?php

namespace Roensby\SymfonyDrupalJsonApi\JsonApi;

interface FilterInterface
{
    /**
     * Getters.
     */

    public function getMemberOf(): ?ConditionGroupInterface;

    public function getOperator(): string;

    public function getPath(): string;

    public function getValue(): string;

    /**
     * Setters.
     */

    public function setMemberOf(?ConditionGroupInterface $conditionGroup): FilterInterface;

    public function setOperator(string $operator): FilterInterface;

    public function setPath(string $path): FilterInterface;

    public function setValue(string $value): FilterInterface;
}
