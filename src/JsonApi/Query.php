<?php declare(strict_types = 1);

namespace Roensby\SymfonyDrupalJsonApi\JsonApi;

use Doctrine\Common\Annotations\AnnotationException;
use Roensby\SymfonyDrupalJsonApi\Constant;
use Roensby\SymfonyDrupalJsonApi\Entity\Base\EntityBaseInterface;
use Roensby\SymfonyDrupalJsonApi\Exception\EntityException;
use Roensby\SymfonyDrupalJsonApi\Exception\EntityMalformedException;
use Roensby\SymfonyDrupalJsonApi\Exception\EntityNotFoundException;
use Roensby\SymfonyDrupalJsonApi\Security\OAuthToken\OAuthTokenInterface;
use Roensby\SymfonyDrupalJsonApi\Security\User\OAuthUserInterface;
use Roensby\SymfonyDrupalJsonApi\Serializer\JsonApiDenormalizer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\PropertyInfo\Extractor\PhpDocExtractor;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\PropertyInfo\PropertyInfoExtractor;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Serializer;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class Query implements QueryInterface
{
    /**
     * @var string
     */
    protected $class;

    /**
     * @var HttpClientInterface
     */
    protected $client;

    /**
     * @var ?int
     */
    protected $count;

    /**
     * @var string
     */
    protected $endpoint;

    /**
     * @var Filter[]
     */
    protected $filters = [];

    /**
     * @var IncludePath[]
     */
    protected $includePaths = [];

    /**
     * @var int
     */
    protected $limit = Constant::JSONAPI_LIMIT;

    /**
     * @var int
     */
    protected $offset = 0;

    /**
     * @var PropertyInfoExtractor
     */
    protected $propertyInfoExtractor;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var ResourceVersion
     */
    protected $resourceVersion;

    /**
     * @var array
     */
    protected $sortOrders = [];

    /**
     * @var Security
     */
    protected $security;

    /**
     * @var ?string
     */
    protected $uuid;

    public function __construct(string $class, HttpClientInterface $client, RequestStack $requestStack, Security $security, string $endpoint)
    {
        $this->class = $class;
        $this->client = $client;
        $this->endpoint = $endpoint;
        $phpDocExtractor = new PhpDocExtractor();
        $reflectionExtractor = new ReflectionExtractor();
        $listExtractors = [$reflectionExtractor];
        $typeExtractors = [$phpDocExtractor, $reflectionExtractor];
        $descriptionExtractors = [$phpDocExtractor];
        $accessExtractors = [$reflectionExtractor];
        $propertyInitializableExtractors = [$reflectionExtractor];
        $this->propertyInfoExtractor = new PropertyInfoExtractor($listExtractors, $typeExtractors, $descriptionExtractors, $accessExtractors, $propertyInitializableExtractors);
        $this->request = $requestStack->getCurrentRequest();
        $this->security = $security;
    }

    public function addFilter(Filter $filter): QueryInterface
    {
        $this->filters[] = $filter;
        return $this;
    }

    public function addIncludePath(IncludePath $path): QueryInterface
    {
        $this->includePaths[] = $path;
        return $this;
    }

    public function addSortOrder(SortOrder $sortOrder): QueryInterface
    {
        $this->sortOrders[] = $sortOrder;
        return $this;
    }

    public function getCount(): ?int
    {
        return $this->count;
    }

    /**
     * @throws \Roensby\SymfonyDrupalJsonApi\Exception\EntityException
     * @throws \Roensby\SymfonyDrupalJsonApi\Exception\EntityMalformedException
     * @throws \Roensby\SymfonyDrupalJsonApi\Exception\EntityNotFoundException
     */
    public function load(): ?EntityBaseInterface
    {
        $this->limit = 1;
        $entities = $this->loadMultiple();
        return empty($entities) ? null : array_pop($entities);
    }

    /**
     * @throws \Roensby\SymfonyDrupalJsonApi\Exception\EntityException
     * @throws \Roensby\SymfonyDrupalJsonApi\Exception\EntityMalformedException
     * @throws \Roensby\SymfonyDrupalJsonApi\Exception\EntityNotFoundException
     */
    public function loadMultiple(): array
    {
        $entities = [];

        // Construct JSON:API query.
        $query = [
            'page[limit]' => $this->limit,
            'page[offset]' => $this->offset,
        ];

        // Add include paths if any.
        $includePaths = [];
        if (!empty($this->includePaths)) {
            foreach ($this->includePaths as $includePath) {
                $includePaths[] = $includePath->getPath();
            }
            $query['include'] = implode(',', $includePaths);
        }

        // Add filters, if any.
        /** @var FilterInterface $filter */
        foreach ($this->filters as $filter) {
            $filterName = md5(implode('.', [$filter->getPath(), $filter->getOperator(), $filter->getValue(), $filter->getMemberOf() ? $filter->getMemberOf()->getName() : null]));
            $query["filter[$filterName][condition][operator]"] = $filter->getOperator();
            $query["filter[$filterName][condition][path]"] = $filter->getPath();
            if (in_array($filter->getOperator(), ['IN', 'NOT IN'])) {
                $query["filter[$filterName][condition][value]"] = explode(',', $filter->getValue());
            } elseif (!in_array($filter->getOperator(), ['IS NULL', 'IS NOT NULL'])) {
                $query["filter[$filterName][condition][value]"] = $filter->getValue();
            }
            if (!empty($filter->getMemberOf())) {
                $conditionGroup = $filter->getMemberOf();
                $conditionGroupName = $conditionGroup->getName();
                // Condition groups may be nested in other condition groups.
                $nestedConditionGroup = $conditionGroup->getMemberOf();
                if ($nestedConditionGroup !== null) {
                    $query["filter[$conditionGroupName][group][memberOf]"] = $nestedConditionGroup->getName();
                    while ($nestedConditionGroup !== null) {
                        $nestedConditionGroupName = $nestedConditionGroup->getName();
                        $query["filter[$nestedConditionGroupName][group][conjunction]"] = $nestedConditionGroup->getConjunction();
                        $nestedConditionGroup = $nestedConditionGroup->getMemberOf();
                        if ($nestedConditionGroup !== null) {
                            $query["filter[$nestedConditionGroupName][group][memberOf]"] = $nestedConditionGroup->getName();
                        }
                    }
                }
                $query["filter[$conditionGroupName][group][conjunction]"] = $conditionGroup->getConjunction();
                $query["filter[$filterName][condition][memberOf]"] = $conditionGroupName;
            }
        }

        // Add sort orders, if any.
        /** @var SortOrderInterface $sortOrder */
        foreach ($this->sortOrders as $sortOrder) {
            $sortOrderName = md5(implode('.', [$sortOrder->getPath(), $sortOrder->getDirection()]));
            $query["sort[$sortOrderName][direction]"] = $sortOrder->getDirection();
            $query["sort[$sortOrderName][path]"] = $sortOrder->getPath();
        }

        // Add resource version, if any.
        if (!empty($this->resourceVersion)) {
            $query['resourceVersion'] = $this->resourceVersion->getVersionIdentifier();
        }

        // Get the OAuth token.
        $user = null;
        if ($this->request->hasPreviousSession()) {
            /** @var OAuthUserInterface $user */
            $user = $this->security->getUser();
        }
        /** @var OAuthTokenInterface $oAuthToken */
        $oAuthToken = $user === null ? $user : $user->getOAuthToken();

        /** @var EntityBaseInterface $entityClass */
        $entityClass = new $this->class;
        $uri = empty($this->uuid) ?
            sprintf('%s/%s/%s', $this->endpoint, $entityClass->getEntityType(), $entityClass->getEntityBundle()) :
            sprintf('%s/%s/%s/%s', $this->endpoint, $entityClass->getEntityType(), $entityClass->getEntityBundle(), $this->uuid);

        // Construct headers.
        $headers = [
            'Accept' => Constant::JSONAPI_HEADER_ACCEPT,
        ];
        if ($oAuthToken !== null) {
            $headers['Authorization'] = sprintf('Bearer %s', $oAuthToken->getAccessToken());
        }

        // Call the Drupal JSON:API endpoint.
        try {
            $response = $this->client->request(Constant::JSONAPI_REQUEST_METHOD_GET, $uri, [
                'headers' => $headers,
                'query' => $query,
            ]);
            $data = $response->getContent();
            $serializer = new Serializer([new JsonApiDenormalizer()], [new JsonEncoder()]);
            $deserializedData = $serializer->deserialize($data, $this->class, 'json', [
                'includePaths' => $includePaths,
            ]);
            $entities = $deserializedData['entities'];
            $this->count = $deserializedData['count'];
        } catch (AnnotationException $exception) {
            throw new EntityMalformedException($exception->getMessage());
        } catch (ClientExceptionInterface $exception) {
            switch ($exception->getCode()) {
                case 404:
                    if (empty($this->uuid)) {
                        throw new EntityNotFoundException(sprintf('A 404 (Not Found) response was received. No %s entities exists or the JSON:API endpoint (%s) was not found.', $entityClass->getEntityType(), $this->endpoint));
                    } else {
                        throw new EntityNotFoundException(sprintf('A 404 (Not Found) response was received. The %s entity %s does not exist or the JSON:API endpoint (%s) was not found.', $entityClass->getEntityType(), $this->uuid, $this->endpoint));
                    }
                    break;

                default:
                    throw new EntityException(sprintf('Attempted to perform a %s request. More information: %s', Constant::JSONAPI_REQUEST_METHOD_GET, $exception->getMessage()));
            }
        } catch (RedirectionExceptionInterface $exception) {
            throw new EntityException($exception->getMessage());
        } catch (ServerExceptionInterface $exception) {
            throw new EntityException($exception->getMessage());
        } catch (TransportExceptionInterface $exception) {
            throw new EntityException($exception->getMessage());
        }
        return $entities;
    }

    public function setLimit(int $limit): QueryInterface
    {
        $this->limit = $limit;
        return $this;
    }

    public function setOffset(int $offset): QueryInterface
    {
        $this->offset = $offset;
        return $this;
    }

    public function setResourceVersion(?ResourceVersionInterface $resourceVersion): QueryInterface
    {
        $this->resourceVersion = $resourceVersion;
        return $this;
    }

    public function setUuid(string $uuid): QueryInterface
    {
        $this->uuid = $uuid;
        return $this;
    }
}
