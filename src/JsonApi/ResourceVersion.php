<?php declare(strict_types = 1);

namespace Roensby\SymfonyDrupalJsonApi\JsonApi;

use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Mapping\ClassMetadata;

/**
 * For more information, see
 * https://www.drupal.org/docs/8/modules/jsonapi/revisions.
 */
class ResourceVersion implements ResourceVersionInterface
{
    /**
     * @var string
     */
    protected $versionIdentifier = 'rel:latest-version';

    public function __construct(?int $revisionId)
    {
        if (!empty($revisionId)) {
            $this->versionIdentifier = sprintf('id:%d', $revisionId);
        }
    }

    /**
     * Getters.
     */

    public function getVersionIdentifier(): string
    {
        return $this->versionIdentifier;
    }

    /**
     * Validation.
     *
     * @param ClassMetadata $metadata
     */
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('revisionId', new NotBlank());
    }

    /**
     * Setters.
     */

    public function setLatestVersion(): ResourceVersionInterface
    {
        $this->versionIdentifier = 'rel:latest-version';
        return $this;
    }

    public function setRevisionId(int $revisionId): ResourceVersionInterface
    {
        $this->versionIdentifier = sprintf('id:%d', $revisionId);
        return $this;
    }

    public function setWorkingCopy(): ResourceVersionInterface
    {
        $this->versionIdentifier = 'rel:working-copy';
        return $this;
    }
}
