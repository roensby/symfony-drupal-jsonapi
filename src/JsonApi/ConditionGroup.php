<?php declare(strict_types = 1);

namespace Roensby\SymfonyDrupalJsonApi\JsonApi;

use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Mapping\ClassMetadata;

class ConditionGroup implements ConditionGroupInterface
{
    /**
     * @var string
     */
    protected $conjunction;

    /**
     * @var ?ConditionGroup
     */
    protected $memberOf;

    /**
     * @var string
     */
    protected $name;

    public function __construct(string $conjunction = 'OR', string $name = 'undefined')
    {
        $this->conjunction = $conjunction;
        $this->name = $name;
    }

    /**
     * Getters.
     */

    public function getConjunction(): string
    {
        return $this->conjunction;
    }

    public function getMemberOf(): ?ConditionGroupInterface
    {
        return $this->memberOf;
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Validation.
     *
     * @param ClassMetadata $metadata
     */
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('conjunction', new NotBlank());
        $metadata->addPropertyConstraint('name', new NotBlank());
    }

    /**
     * Setters.
     */

    public function setConjunction(string $conjunction): ConditionGroupInterface
    {
        $this->conjunction = $conjunction;
        return $this;
    }

    public function setMemberOf(?ConditionGroupInterface $conditionGroup): ConditionGroupInterface
    {
        $this->memberOf = $conditionGroup;
        return $this;
    }

    public function setName(string $name): ConditionGroupInterface
    {
        $this->name = $name;
        return $this;
    }
}
