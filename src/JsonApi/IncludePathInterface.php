<?php

namespace Roensby\SymfonyDrupalJsonApi\JsonApi;

interface IncludePathInterface
{
    /**
     * Getters.
     */

    public function getPath(): string;

    /**
     * Setters.
     */

    public function setPath(string $path): IncludePathInterface;
}
