<?php

namespace Roensby\SymfonyDrupalJsonApi\JsonApi;

interface ResourceVersionInterface
{
    /**
     * Getters.
     */

    public function getVersionIdentifier(): string;

    /**
     * Setters.
     */

    public function setLatestVersion(): ResourceVersionInterface;

    public function setRevisionId(int $revisionId): ResourceVersionInterface;

    public function setWorkingCopy(): ResourceVersionInterface;
}
