<?php

namespace Roensby\SymfonyDrupalJsonApi\JsonApi;

interface ConditionGroupInterface
{
    /**
     * Getters.
     */

    public function getConjunction(): string;

    public function getMemberOf(): ?ConditionGroupInterface;

    public function getName(): string;

    /**
     * Setters.
     */

    public function setConjunction(string $conjunction): ConditionGroupInterface;

    public function setMemberOf(?ConditionGroupInterface $conditionGroup): ConditionGroupInterface;

    public function setName(string $name): ConditionGroupInterface;
}
