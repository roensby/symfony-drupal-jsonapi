<?php declare(strict_types = 1);

namespace Roensby\SymfonyDrupalJsonApi\Storage;

use Doctrine\Common\Annotations\AnnotationException;
use Roensby\SymfonyDrupalJsonApi\Constant;
use Roensby\SymfonyDrupalJsonApi\Entity\Base\EntityBaseInterface;
use Roensby\SymfonyDrupalJsonApi\Entity\Core\File;
use Roensby\SymfonyDrupalJsonApi\Entity\Core\FileInterface;
use Roensby\SymfonyDrupalJsonApi\Exception\CsrfTokenException;
use Roensby\SymfonyDrupalJsonApi\Exception\EntityException;
use Roensby\SymfonyDrupalJsonApi\Exception\EntityMalformedException;
use Roensby\SymfonyDrupalJsonApi\Exception\EntityNotFoundException;
use Roensby\SymfonyDrupalJsonApi\Security\OAuthToken\OAuthTokenInterface;
use Roensby\SymfonyDrupalJsonApi\Security\User\OAuthUserInterface;
use Roensby\SymfonyDrupalJsonApi\Serializer\JsonApiDenormalizer;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class FileStorage
{
    /**
     * @var HttpClientInterface
     */
    protected $client;

    /**
     * @var string
     */
    protected $csrfEndpoint;

    /**
     * @var string
     */
    protected $endpoint;

    /**
     * @var Security
     */
    protected $security;

    public function __construct(array $configuration, HttpClientInterface $client, Security $security)
    {
        $this->client = $client;
        $this->endpoint = $configuration['endpoint'];
        $this->csrfEndpoint = $configuration['csrf_endpoint'];
        $this->security = $security;
    }

    /**
     * @throws CsrfTokenException
     * @throws EntityException
     * @throws EntityMalformedException
     * @throws EntityNotFoundException
     */
    public function create(UploadedFile $uploadedFile, EntityBaseInterface $parentEntity, string $parentFieldName): ?FileInterface
    {
        $file = null;

        // Get the OAuth token.
        /** @var OAuthUserInterface $user */
        $user = $this->security->getUser();
        /** @var OAuthTokenInterface $oAuthToken */
        $oAuthToken = empty($user) ? null : $user->getOAuthToken();

        // Get CSRF token.
        $csrfToken = null;
        try {
            $response = $this->client->request('GET', $this->csrfEndpoint);
            $csrfToken = $response->getContent();
            if (empty($csrfToken)) {
                throw new CsrfTokenException(sprintf('Attempted to perform a %s request, but could not retrieve a CSRF token.', Constant::JSONAPI_REQUEST_METHOD_POST));
            }
        } catch (TransportExceptionInterface $exception) {
            throw new CsrfTokenException($exception->getMessage());
        } catch (ClientExceptionInterface $exception) {
            switch ($exception->getCode()) {
                case 404:
                    throw new CsrfTokenException(sprintf('A 404 (Not Found) response was received. The CSRF endpoint (%s) was not found.', $this->csrfEndpoint));
                    break;

                default:
                    throw new CsrfTokenException(sprintf('A %s response was received. The Drupal installation may not be configured correctly. More information: %s', $exception->getCode(), $exception->getMessage()));
            }
        } catch (RedirectionExceptionInterface $exception) {
            throw new CsrfTokenException($exception->getMessage());
        } catch (ServerExceptionInterface $exception) {
            throw new CsrfTokenException($exception->getMessage());
        }

        // Perform file storage request.
        $uri = empty($parentEntity->getUuid()) ?
            sprintf('%s/%s/%s/%s', $this->endpoint, $parentEntity->getEntityType(), $parentEntity->getEntityBundle(), $parentFieldName) :
            sprintf('%s/%s/%s/%s/%s', $this->endpoint, $parentEntity->getEntityType(), $parentEntity->getEntityBundle(), $parentEntity->getUuid(), $parentFieldName);
        $resourceId = $parentEntity->getUuid() !== null ? sprintf('; resource-id={%s}', $parentEntity->getUuid()) : '';

        // Construct headers.
        $headers = [
            'Content-Type' => Constant::JSONAPI_HEADER_CONTE_TYPE_FILE,
            'Accept' => Constant::JSONAPI_HEADER_ACCEPT,
            'X-CSRF-Token' => $csrfToken,
            'Content-Disposition' => sprintf('file; filename="%s"', preg_replace("([^\w\s\d\-_~,;\[\]\(\).])", '', $uploadedFile->getClientOriginalName())) . $resourceId,
        ];
        if ($oAuthToken !== null) {
            $headers['Authorization'] = sprintf('Bearer %s', $oAuthToken->getAccessToken());
        }

        try {
            $response = $this->client->request(Constant::JSONAPI_REQUEST_METHOD_POST, $uri, [
                'headers' => $headers,
                'body' => file_get_contents($uploadedFile->getRealPath()),
            ]);
            $data = $response->getContent();
            if (!empty($data)) {
                $denormalizerSerializer = new Serializer([new JsonApiDenormalizer()], [new JsonEncoder()]);
                $denormalizedEntities = $denormalizerSerializer->deserialize($data, File::class, 'json', [
                    'groups' => ['attributes', 'id', 'relationships'],
                ]);
                if (!empty($denormalizedEntities['entities'])) {
                    $file = array_pop($denormalizedEntities['entities']);
                }
            }
        } catch (AnnotationException $exception) {
            throw new EntityMalformedException($exception->getMessage());
        } catch (ClientExceptionInterface $exception) {
            switch ($exception->getCode()) {
                case 404:
                    if (empty($parentEntity->getUuid())) {
                        throw new EntityException(sprintf('A 404 (Not Found) response was received. The %s field of the %s entity or the JSON:API endpoint (%s) was not found.', $parentFieldName, $parentEntity->getEntityBundle(), $this->endpoint));
                    } else {
                        throw new EntityNotFoundException(sprintf('A 404 (Not Found) response was received. The %s field of the %s entity %s does not exist or the JSON:API endpoint (%s) was not found.', $parentFieldName, $parentEntity->getEntityBundle(), $parentEntity->getUuid(), $this->endpoint));
                    }
                    break;

                case 422:
                    throw new EntityMalformedException(sprintf('Attempted to perform a %s request. More information: %s', Constant::JSONAPI_REQUEST_METHOD_POST, $exception->getMessage()));
                    break;

                default:
                    throw new EntityException(sprintf('Attempted to perform a %s request. More information: %s', Constant::JSONAPI_REQUEST_METHOD_POST, $exception->getMessage()));
            }
        } catch (RedirectionExceptionInterface $exception) {
            throw new EntityException($exception->getMessage());
        } catch (ServerExceptionInterface $exception) {
            throw new EntityException($exception->getMessage());
        } catch (TransportExceptionInterface $exception) {
            throw new EntityException($exception->getMessage());
        } catch (ExceptionInterface $exception) {
            throw new EntityMalformedException(sprintf('Failed to deserialize entity of class %s. More information: %s', File::class, $exception->getMessage()));
        }
        return $file;
    }
}
