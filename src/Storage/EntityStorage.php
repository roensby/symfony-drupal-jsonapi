<?php declare(strict_types = 1);

namespace Roensby\SymfonyDrupalJsonApi\Storage;

use Doctrine\Common\Annotations\AnnotationException;
use Roensby\SymfonyDrupalJsonApi\Constant;
use Roensby\SymfonyDrupalJsonApi\Entity\Base\EntityBaseInterface;
use Roensby\SymfonyDrupalJsonApi\Exception\CsrfTokenException;
use Roensby\SymfonyDrupalJsonApi\Exception\EntityMalformedException;
use Roensby\SymfonyDrupalJsonApi\Exception\EntityNotFoundException;
use Roensby\SymfonyDrupalJsonApi\Exception\EntityException;
use Roensby\SymfonyDrupalJsonApi\Security\OAuthToken\OAuthTokenInterface;
use Roensby\SymfonyDrupalJsonApi\Security\User\OAuthUserInterface;
use Roensby\SymfonyDrupalJsonApi\Serializer\JsonApiDenormalizer;
use Roensby\SymfonyDrupalJsonApi\Serializer\JsonApiNormalizer;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class EntityStorage
{
    /**
     * @var HttpClientInterface
     */
    protected $client;

    /**
     * @var string
     */
    protected $csrfEndpoint;

    /**
     * @var string
     */
    protected $endpoint;

    /**
     * @var Security
     */
    protected $security;

    public function __construct(array $configuration, HttpClientInterface $client, Security $security)
    {
        $this->client = $client;
        $this->endpoint = $configuration['endpoint'];
        $this->csrfEndpoint = $configuration['csrf_endpoint'];
        $this->security = $security;
    }

    /**
     * @throws \Roensby\SymfonyDrupalJsonApi\Exception\CsrfTokenException
     * @throws \Roensby\SymfonyDrupalJsonApi\Exception\EntityException
     * @throws \Roensby\SymfonyDrupalJsonApi\Exception\EntityMalformedException
     * @throws \Roensby\SymfonyDrupalJsonApi\Exception\EntityNotFoundException
     */
    public function create(EntityBaseInterface $entity): ?EntityBaseInterface
    {
        $uri = sprintf('%s/%s/%s', $this->endpoint, $entity->getEntityType(), $entity->getEntityBundle());
        return $this->request($entity, Constant::JSONAPI_REQUEST_METHOD_POST, $uri);
    }

    /**
     * @throws \Roensby\SymfonyDrupalJsonApi\Exception\CsrfTokenException
     * @throws \Roensby\SymfonyDrupalJsonApi\Exception\EntityException
     * @throws \Roensby\SymfonyDrupalJsonApi\Exception\EntityMalformedException
     * @throws \Roensby\SymfonyDrupalJsonApi\Exception\EntityNotFoundException
     */
    public function update(EntityBaseInterface $entity): ?EntityBaseInterface
    {
        if (empty($entity->getUuid())) {
            $uri = sprintf('%s/%s/%s', $this->endpoint, $entity->getEntityType(), $entity->getEntityBundle());
            return $this->request($entity, Constant::JSONAPI_REQUEST_METHOD_POST, $uri);
        } else {
            $uri = sprintf('%s/%s/%s/%s', $this->endpoint, $entity->getEntityType(), $entity->getEntityBundle(), $entity->getUuid());
            return $this->request($entity, Constant::JSONAPI_REQUEST_METHOD_PATCH, $uri);
        }
    }

    /**
     * @throws \Roensby\SymfonyDrupalJsonApi\Exception\CsrfTokenException
     * @throws \Roensby\SymfonyDrupalJsonApi\Exception\EntityException
     * @throws \Roensby\SymfonyDrupalJsonApi\Exception\EntityMalformedException
     * @throws \Roensby\SymfonyDrupalJsonApi\Exception\EntityNotFoundException
     */
    public function delete(EntityBaseInterface $entity): ?EntityBaseInterface
    {
        $uri = sprintf('%s/%s/%s/%s', $this->endpoint, $entity->getEntityType(), $entity->getEntityBundle(), $entity->getUuid());
        return $this->request($entity, Constant::JSONAPI_REQUEST_METHOD_DELETE, $uri);
    }

    /**
     * @throws \Roensby\SymfonyDrupalJsonApi\Exception\CsrfTokenException
     * @throws \Roensby\SymfonyDrupalJsonApi\Exception\EntityException
     * @throws \Roensby\SymfonyDrupalJsonApi\Exception\EntityMalformedException
     * @throws \Roensby\SymfonyDrupalJsonApi\Exception\EntityNotFoundException
     */
    protected function request(EntityBaseInterface $entity, string $method, string $uri): ?EntityBaseInterface
    {
        // Serialize the entity.
        try {
            $normalizerSerializer = new Serializer([new JsonApiNormalizer()], [new JsonEncoder()]);
            $data = $normalizerSerializer->serialize($entity, 'json', [
                'groups' => ['attributes', 'id', 'relationships']
            ]);
        } catch (AnnotationException $exception) {
            throw new EntityException($exception->getMessage());
        } catch (ExceptionInterface $exception) {
            throw new EntityMalformedException(sprintf('Failed to serialize entity of class %s. More information: %s', get_class($entity), $exception->getMessage()));
        }

        // Get the OAuth token.
        /** @var OAuthUserInterface $user */
        $user = $this->security->getUser();
        /** @var OAuthTokenInterface $oAuthToken */
        $oAuthToken = empty($user) ? null : $user->getOAuthToken();

        // Get CSRF token.
        $csrfToken = null;
        try {
            $response = $this->client->request('GET', $this->csrfEndpoint);
            $csrfToken = $response->getContent();
            if (empty($csrfToken)) {
                throw new CsrfTokenException(sprintf('Attempted to perform a %s request, but could not retrieve a CSRF token.', $method));
            }
        } catch (TransportExceptionInterface $exception) {
            throw new CsrfTokenException($exception->getMessage());
        } catch (ClientExceptionInterface $exception) {
            switch ($exception->getCode()) {
                case 404:
                    throw new CsrfTokenException(sprintf('A 404 (Not Found) response was received. The CSRF endpoint (%s) was not found.', $this->csrfEndpoint));
                    break;

                default:
                    throw new CsrfTokenException(sprintf('A %s response was received. The Drupal installation may not be configured correctly. More information: %s', $exception->getCode(), $exception->getMessage()));
            }
        } catch (RedirectionExceptionInterface $exception) {
            throw new CsrfTokenException($exception->getMessage());
        } catch (ServerExceptionInterface $exception) {
            throw new CsrfTokenException($exception->getMessage());
        }

        // Construct headers.
        $headers = [
            'Content-Type' => Constant::JSONAPI_HEADER_CONTENT_TYPE,
            'Accept' => Constant::JSONAPI_HEADER_ACCEPT,
            'X-CSRF-Token' => $csrfToken,
        ];
        if ($oAuthToken !== null) {
            $headers['Authorization'] = sprintf('Bearer %s', $oAuthToken->getAccessToken());
        }

        // Perform entity storage request.
        try {
            $response = $this->client->request($method, $uri, [
                'headers' => $headers,
                'body' => $data,
            ]);
            $data = $response->getContent();
            if (!empty($data)) {
                $denormalizerSerializer = new Serializer([new JsonApiDenormalizer()], [new JsonEncoder()]);
                $denormalizedEntities = $denormalizerSerializer->deserialize($data, get_class($entity), 'json', [
                    'groups' => ['id'],
                ]);
                if (!empty($denormalizedEntities['entities'])) {
                    $denormalizedEntity = array_pop($denormalizedEntities['entities']);
                    $entity->setUuid($denormalizedEntity->getUuid());
                }
            }
        } catch (AnnotationException $exception) {
            throw new EntityMalformedException($exception->getMessage());
        } catch (ClientExceptionInterface $exception) {
            switch ($exception->getCode()) {
                case 404:
                    if (
                        $method === Constant::JSONAPI_REQUEST_METHOD_DELETE ||
                        $method === Constant::JSONAPI_REQUEST_METHOD_PATCH
                    ) {
                        throw new EntityNotFoundException(sprintf('A 404 (Not Found) response was received. The %s entity %s does not exist or the JSON:API endpoint (%s) was not found.', $entity->getEntityType(), $entity->getUuid(), $this->endpoint));
                    } elseif ($method === Constant::JSONAPI_REQUEST_METHOD_POST) {
                        throw new EntityException(sprintf('A 404 (Not Found) response was received. The %s--%s subpath was not found or the JSON:API endpoint (%s) was not found.', $entity->getEntityType(), $entity->getEntityBundle(), $this->endpoint));
                    }
                    break;

                case 422:
                    throw new EntityMalformedException(sprintf('Attempted to perform a %s request. More information: %s', $method, $exception->getMessage()));
                    break;

                default:
                    throw new EntityException(sprintf('Attempted to perform a %s request. More information: %s', $method, $exception->getMessage()));
            }
        } catch (RedirectionExceptionInterface $exception) {
            throw new EntityException($exception->getMessage());
        } catch (ServerExceptionInterface $exception) {
            throw new EntityException($exception->getMessage());
        } catch (TransportExceptionInterface $exception) {
            throw new EntityException($exception->getMessage());
        } catch (ExceptionInterface $exception) {
            throw new EntityMalformedException(sprintf('Failed to deserialize entity of class %s. More information: %s', get_class($entity), $exception->getMessage()));
        }
        return $entity;
    }
}
