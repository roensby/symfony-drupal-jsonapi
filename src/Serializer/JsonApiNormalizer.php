<?php declare(strict_types = 1);

namespace Roensby\SymfonyDrupalJsonApi\Serializer;

use DateTime;
use Doctrine\Common\Annotations\AnnotationException;
use Doctrine\Common\Annotations\AnnotationReader;
use Roensby\SymfonyDrupalJsonApi\Constant;
use Roensby\SymfonyDrupalJsonApi\Entity\Base\EntityBase;
use Roensby\SymfonyDrupalJsonApi\Entity\Base\EntityBaseInterface;
use ReflectionClass;
use ReflectionException;
use Symfony\Component\PropertyInfo\Extractor\PhpDocExtractor;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\PropertyInfo\PropertyInfoExtractor;
use Symfony\Component\PropertyInfo\Type;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Component\Serializer\NameConverter\MetadataAwareNameConverter;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class JsonApiNormalizer implements ContextAwareNormalizerInterface
{

    /**
     * @var AnnotationReader
     */
    protected $annotationReader;

    /**
     * @var CamelCaseToSnakeCaseNameConverter
     */
    protected $camelCaseToSnakeCaseNameConverter;

    /**
     * @var MetadataAwareNameConverter
     */
    protected $metadataAwareNameConverter;

    /**
     * @var ObjectNormalizer
     */
    protected $objectNormalizer;

    /**
     * @var PropertyInfoExtractor
     */
    protected $propertyInfoExtractor;

    /**
     * @throws AnnotationException
     */
    public function __construct()
    {
        $this->annotationReader = new AnnotationReader();
        $this->camelCaseToSnakeCaseNameConverter = new CamelCaseToSnakeCaseNameConverter();
        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader($this->annotationReader));
        $this->metadataAwareNameConverter = new MetadataAwareNameConverter($classMetadataFactory);
        $this->objectNormalizer = new ObjectNormalizer($classMetadataFactory, $this->metadataAwareNameConverter);
        $phpDocExtractor = new PhpDocExtractor();
        $reflectionExtractor = new ReflectionExtractor();
        $listExtractors = [$reflectionExtractor];
        $typeExtractors = [$phpDocExtractor, $reflectionExtractor];
        $descriptionExtractors = [$phpDocExtractor];
        $accessExtractors = [$reflectionExtractor];
        $propertyInitializableExtractors = [$reflectionExtractor];
        $this->propertyInfoExtractor = new PropertyInfoExtractor($listExtractors, $typeExtractors, $descriptionExtractors, $accessExtractors, $propertyInitializableExtractors);
    }

    /**
     * @throws ExceptionInterface
     * @throws ReflectionException
     */
    public function normalize($object, $format = null, array $context = [])
    {
        /** @var EntityBaseInterface $object */
        if ($object->getUuid() !== null) {
            $data['data']['id'] = $object->getUuid();
        }
        $data['data']['type'] = sprintf('%s--%s', $object->getEntityType(), $object->getEntityBundle());
        $data['data']['attributes'] = $this->normalizeAttributes($object);
        $data['data']['relationships'] = $this->normalizeRelationships($object);
        return $data;
    }

    /**
     * @throws ReflectionException
     */
    protected function normalizeAttributes(EntityBaseInterface $object)
    {
        $attributes = [];
        $class = get_class($object);
        $reflectionClass = new ReflectionClass($class);
        $properties = $reflectionClass->getProperties();
        foreach ($properties as $property) {
            /** @var Groups $propertyGroups */
            $propertyGroups = $this->annotationReader->getPropertyAnnotation($property, Groups::class);
            if (
                !empty($propertyGroups) &&
                in_array('attributes', $propertyGroups->getGroups())
            ) {
                $property->setAccessible(true);
                $attributeName = $this->camelCaseToSnakeCaseNameConverter->normalize($this->metadataAwareNameConverter->normalize($property->getName(), $class));
                $attributeValue = $property->getValue($object);
                if (
                    $attributeName !== null &&
                    $attributeValue !== null
                ) {
                    $propertyTypes = $this->propertyInfoExtractor->getTypes($class, $property->getName());
                    /** @var Type $propertyType */
                    foreach ($propertyTypes as $propertyType) {
                        // Convert DateTime objects to ISO 8601 formatted strings.
                        if ($propertyType->getClassName() === DateTime::class) {
                            /** @var DateTime $attributeValue */
                            $attributes[$attributeName] = $attributeValue->format(Constant::JSONAPI_ISO_8601_DATETIME_FORMAT);
                        }
                        // Handle arrays, scalars and other objects.
                        else {
                            $attributes[$attributeName] = $attributeValue;
                        }
                    }
                }
            }
        }
        return $attributes;
    }

    /**
     * @throws ReflectionException
     */
    protected function normalizeRelationships(EntityBaseInterface $object)
    {
        $relationships = [];
        $class = get_class($object);
        $reflectionClass = new ReflectionClass($class);
        $properties = $reflectionClass->getProperties();
        foreach ($properties as $property) {
            $propertyTypes = $this->propertyInfoExtractor->getTypes($class, $property->getName());
            /** @var Type $propertyType */
            foreach ($propertyTypes as $propertyType) {
                // Entity reference fields with cardinality 1.
                if (
                    $propertyType->getClassName() !== null &&
                    is_subclass_of($propertyType->getClassName(), EntityBase::class)
                ) {
                    $property->setAccessible(true);
                    $relationshipName = $this->camelCaseToSnakeCaseNameConverter->normalize($this->metadataAwareNameConverter->normalize($property->getName(), $class));
                    /** @var EntityBaseInterface $referencedEntity */
                    $referencedEntity = $property->getValue($object);
                    if ($referencedEntity === []) {
                        $relationships[$relationshipName] = [];
                    } elseif (
                        $referencedEntity !== null &&
                        get_class($referencedEntity) === $propertyType->getClassName()
                    ) {
                        $relationships[$relationshipName] = [
                            'data' => [
                                'type' => sprintf('%s--%s', $referencedEntity->getEntityType(), $referencedEntity->getEntityBundle()),
                                'id' => $referencedEntity->getUuid(),
                            ],
                        ];
                    }
                    // Entity reference fields with cardinality > 1.
                } elseif (
                    $propertyType->isCollection() &&
                    $propertyType->getCollectionValueType() !== null &&
                    $propertyType->getCollectionValueType()->getClassName() !== null &&
                    is_subclass_of($propertyType->getCollectionValueType()->getClassName(), EntityBase::class)
                ) {
                    $property->setAccessible(true);
                    $relationshipName = $this->camelCaseToSnakeCaseNameConverter->normalize($this->metadataAwareNameConverter->normalize($property->getName(), $class));
                    /** @var EntityBaseInterface[] $referencedEntities */
                    $referencedEntities = $property->getValue($object);
                    if ($referencedEntities === []) {
                        $relationships[$relationshipName] = [];
                    } elseif ($referencedEntities !== null) {
                        foreach ($referencedEntities as $referencedEntity) {
                            if (get_class($referencedEntity) === $propertyType->getCollectionValueType()->getClassName()) {
                                $relationships[$relationshipName]['data'][] = [
                                    'type' => sprintf('%s--%s', $referencedEntity->getEntityType(), $referencedEntity->getEntityBundle()),
                                    'id' => $referencedEntity->getUuid(),
                                ];
                            }
                        }
                    }
                }
            }
        }
        return $relationships;
    }

    public function supportsNormalization($data, $format = null, array $context = [])
    {
        return is_subclass_of($data, EntityBase::class);
    }
}
