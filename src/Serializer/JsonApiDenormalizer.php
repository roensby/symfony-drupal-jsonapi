<?php declare(strict_types = 1);

namespace Roensby\SymfonyDrupalJsonApi\Serializer;

use Doctrine\Common\Annotations\AnnotationException;
use Doctrine\Common\Annotations\AnnotationReader;
use Roensby\SymfonyDrupalJsonApi\Entity\Base\EntityBase;
use Roensby\SymfonyDrupalJsonApi\Entity\Base\EntityBaseInterface;
use ReflectionClass;
use ReflectionException;
use Roensby\SymfonyDrupalJsonApi\JsonApi\IncludePath;
use Symfony\Component\PropertyInfo\Extractor\PhpDocExtractor;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\PropertyInfo\PropertyInfoExtractor;
use Symfony\Component\PropertyInfo\Type;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Component\Serializer\NameConverter\MetadataAwareNameConverter;
use Symfony\Component\Serializer\Normalizer\ContextAwareDenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class JsonApiDenormalizer implements ContextAwareDenormalizerInterface
{
    /**
     * @var CamelCaseToSnakeCaseNameConverter
     */
    protected $camelCaseToSnakeCaseNameConverter;

    /**
     * @var array
     */
    protected $included = [];

    /**
     * @var ObjectNormalizer
     */
    protected $objectNormalizer;

    /**
     * @var PropertyInfoExtractor
     */
    protected $propertyInfoExtractor;

    /**
     * @throws \Doctrine\Common\Annotations\AnnotationException
     */
    public function __construct()
    {
        $this->camelCaseToSnakeCaseNameConverter = new CamelCaseToSnakeCaseNameConverter();
        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $metadataAwareNameConverter = new MetadataAwareNameConverter($classMetadataFactory, $this->camelCaseToSnakeCaseNameConverter);
        $this->objectNormalizer = new ObjectNormalizer($classMetadataFactory, $metadataAwareNameConverter);
        $phpDocExtractor = new PhpDocExtractor();
        $reflectionExtractor = new ReflectionExtractor();
        $listExtractors = [$reflectionExtractor];
        $typeExtractors = [$phpDocExtractor, $reflectionExtractor];
        $descriptionExtractors = [$phpDocExtractor];
        $accessExtractors = [$reflectionExtractor];
        $propertyInitializableExtractors = [$reflectionExtractor];
        $this->propertyInfoExtractor = new PropertyInfoExtractor($listExtractors, $typeExtractors, $descriptionExtractors, $accessExtractors, $propertyInitializableExtractors);
    }

    /**
     * @throws AnnotationException
     * @throws ExceptionInterface
     * @throws ReflectionException
     */
    public function denormalize($data, $type, $format = null, array $context = [])
    {
        $entities = [];
        $this->included = isset($data['included']) ? $data['included'] : [];
        if (isset($data['data']) && is_int(key($data['data']))) {
            foreach ($data['data'] as $item) {
                $entities[] = $this->recursiveDenormalize($item, $type, $format, $context);
            }
        } elseif (!empty($data['data'])) {
            $entities[] = $this->recursiveDenormalize($data['data'], $type, $format, $context);
        }
        return [
            'entities' => $entities,
            'count' => (isset($data['meta']['count']) && is_numeric($data['meta']['count'])) ? (int) $data['meta']['count'] : null,
        ];
    }

    /**
     * @throws AnnotationException
     * @throws ExceptionInterface
     * @throws ReflectionException
     */
    public function recursiveDenormalize($data, string $type, string $format = null, array $context = [])
    {
        $denormalizedData = null;
        if (
            isset($data['id']) &&
            isset($data['attributes'])
        ) {
            $denormalizedData = $data['attributes'];
            $denormalizedData['id'] = $data['id'];
        }
        if (isset($data['meta'])) {
            $denormalizedData = array_merge($denormalizedData, $data['meta']);
        }
        if (isset($data['relationships'])) {
            foreach ($data['relationships'] as $fieldName => $relationship) {
                // Check for include paths that match field name.
                $included = false;
                if (isset($context['includePaths'])) {
                    /** @var IncludePath $includePath */
                    foreach ($context['includePaths'] as $includePath) {
                        if ($includePath === $fieldName) {
                            $included = true;
                            break;
                        }
                    }
                }
                // Skip fields that aren't specified in the include paths.
                if ($included) {
                    $subContext = $context;
                    $subContext['includePaths'] = [];
                    // Create include paths for this field.
                    foreach ($context['includePaths'] as $includePath) {
                        $subPath = sprintf('%s.', $fieldName);
                        if (strpos($includePath, $subPath) === 0) {
                            $includePath = substr($includePath, strlen($subPath));
                            $subContext['includePaths'][] = $includePath;
                        }
                    }
                    if (isset($relationship['data'])) {
                        // EntityBase reference field with cardinality > 1.
                        if (is_int(key($relationship['data']))) {
                            foreach ($relationship['data'] as $item) {
                                if (
                                    isset($item['id']) &&
                                    isset($item['type'])
                                ) {
                                    $relationshipData = $this->extractRelationship($item['id'], $item['type']);
                                    if (!empty($relationshipData)) {
                                        // Include meta, if any.
                                        if (isset($item['meta'])) {
                                            $relationshipData['meta'] = $item['meta'];
                                        }
                                        $propertyTypes = $this->propertyInfoExtractor->getTypes($type, $this->getDeserializedName($type, $fieldName));
                                        if (!empty($propertyTypes)) {
                                            list(, $bundle) = explode('--', $item['type']);
                                            foreach ($propertyTypes as $propertyType) {
                                                $relationshipClass = $this->getPropertyClass($propertyType);
                                                if ($bundle === $this->getDefaultPropertyValue($relationshipClass, 'bundle')) {
                                                    $denormalizedData[$fieldName][] = $this->recursiveDenormalize($relationshipData, $relationshipClass, $format, $subContext);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        // EntityBase reference field with cardinality = 1.
                        else {
                            if (
                                isset($relationship['data']['id']) &&
                                isset($relationship['data']['type'])
                            ) {
                                $relationshipData = $this->extractRelationship($relationship['data']['id'], $relationship['data']['type']);
                                if (!empty($relationshipData)) {
                                    // Include meta, if any.
                                    if (isset($relationship['data']['meta'])) {
                                        $relationshipData['meta'] = $relationship['data']['meta'];
                                    }
                                    $propertyTypes = $this->propertyInfoExtractor->getTypes($type, $this->getDeserializedName($type, $fieldName));
                                    if (!empty($propertyTypes)) {
                                        list(, $bundle) = explode('--', $relationship['data']['type']);
                                        foreach ($propertyTypes as $propertyType) {
                                            $relationshipClass = $this->getPropertyClass($propertyType);
                                            if ($bundle === $this->getDefaultPropertyValue($relationshipClass, 'bundle')) {
                                                $denormalizedData[$fieldName] = $this->recursiveDenormalize($relationshipData, $relationshipClass, $format, $subContext);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        /** @var EntityBaseInterface $entity */
        $entity = $this->objectNormalizer->denormalize($denormalizedData, $type, $format, [
            'groups' => ['id', 'attributes', 'relationships', 'meta'],
        ]);
        return $entity;
    }

    /**
     * @throws ReflectionException
     */
    public function supportsDenormalization($data, $type, $format = null, array $context = [])
    {
        if (is_subclass_of($type, EntityBase::class) && isset($data['data'])) {
            if (is_int(key($data['data'])) && isset($data['data'][0]['type'])) {
                list($entityType, $entityBundle) = explode('--', $data['data'][0]['type']);
                if (
                    $this->getDefaultPropertyValue($type, 'type') === $entityType &&
                    $this->getDefaultPropertyValue($type, 'bundle') === $entityBundle
                ) {
                    return true;
                }
            } elseif (isset($data['data']['type'])) {
                list($entityType, $entityBundle) = explode('--', $data['data']['type']);
                if (
                    $this->getDefaultPropertyValue($type, 'type') === $entityType &&
                    $this->getDefaultPropertyValue($type, 'bundle') === $entityBundle
                ) {
                    return true;
                }
            } elseif (empty($data['data'])) {
                return true;
            }
        }
        return false;
    }

    private function extractRelationship(string $relationshipId, string $relationshipType): array
    {
        foreach ($this->included as $item) {
            if (
                isset($item['id']) &&
                isset($item['type']) &&
                $relationshipId === $item['id'] &&
                $relationshipType === $item['type']
            ) {
                return $item;
            }
        }
        return [];
    }

    /**
     * @throws ReflectionException
     */
    private function getDefaultPropertyValue(string $class, string $property): ?string
    {
        $value = null;
        $reflectionClass = new ReflectionClass($class);
        $defaultPropertyValues = $reflectionClass->getDefaultProperties();
        if (isset($defaultPropertyValues[$property])) {
            $value = $defaultPropertyValues[$property];
        }
        return $value;
    }

    private function getPropertyClass(Type $propertyType): ?string
    {
        if ($propertyType->isCollection() && $propertyType->getCollectionValueType() !== null) {
            return $propertyType->getCollectionValueType()->getClassName();
        } else {
            return $propertyType->getClassName();
        }
    }

    /**
     * @throws AnnotationException
     * @throws ReflectionException
     */
    private function getDeserializedName(string $class, string $fieldName): ?string
    {
        $annotationReader = new AnnotationReader();
        $reflectionClass = new ReflectionClass($class);
        foreach ($reflectionClass->getProperties() as $reflectionProperty) {
            /** @var SerializedName $serializedName */
            $serializedName = $annotationReader->getPropertyAnnotation($reflectionProperty, SerializedName::class);
            if (
                $serializedName !== null &&
                $fieldName === $serializedName->getSerializedName()
            ) {
                return $reflectionProperty->getName();
            }
        }
        return $this->camelCaseToSnakeCaseNameConverter->denormalize($fieldName);
    }
}
