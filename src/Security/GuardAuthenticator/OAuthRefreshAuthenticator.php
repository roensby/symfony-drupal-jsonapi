<?php

namespace Roensby\SymfonyDrupalJsonApi\Security\GuardAuthenticator;

use Roensby\SymfonyDrupalJsonApi\Constant;
use Roensby\SymfonyDrupalJsonApi\Entity\Core\User;
use Roensby\SymfonyDrupalJsonApi\Entity\Core\UserInterface;
use Roensby\SymfonyDrupalJsonApi\Exception\Base\DrupalJsonApiException;
use Roensby\SymfonyDrupalJsonApi\Factory\EntityFactory;
use Roensby\SymfonyDrupalJsonApi\JsonApi\Filter;
use Roensby\SymfonyDrupalJsonApi\JsonApi\IncludePath;
use Roensby\SymfonyDrupalJsonApi\Security\OAuthToken\OAuthToken;
use Roensby\SymfonyDrupalJsonApi\Security\OAuthToken\OAuthTokenInterface;
use Roensby\SymfonyDrupalJsonApi\Security\User\OAuthUserInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface as SymfonyUserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Validator\Validation;
use Symfony\Contracts\HttpClient\Exception\ExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * This class refreshes expired OAuth2 tokens.
 * For more information and instructions for use, see
 * https://symfony.com/doc/current/security/guard_authentication.html
 */
class OAuthRefreshAuthenticator extends AbstractGuardAuthenticator
{
    /**
     * @var array
     */
    protected $configuration;

    /**
     * @var HttpClientInterface
     */
    protected $httpClient;

    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * @var Security
     */
    protected $security;

    /**
     * @var EntityFactory
     */
    protected $entityFactory;

    public function __construct(array $configuration, EntityFactory $entityFactory, HttpClientInterface $httpClient, RouterInterface $router, Security $security)
    {
        $this->configuration = $configuration;
        $this->entityFactory = $entityFactory;
        $this->httpClient = $httpClient;
        $this->router = $router;
        $this->security = $security;
    }

    public function checkCredentials($credentials, ?SymfonyUserInterface $user)
    {
        if (!is_a($credentials, OAuthTokenInterface::class)) {
            return false;
        }
        $validator = Validation::createValidatorBuilder()
            ->addMethodMapping('loadValidatorMetadata')
            ->getValidator();
        $violations = $validator->validate($credentials);
        return count($violations) === 0;
    }

    public function getCredentials(Request $request)
    {
        /** @var OAuthUserInterface $user */
        $user = $this->security->getUser();
        $oAuthToken = $user->getOAuthToken();
        if ($oAuthToken->isExpired()) {
            try {
                $response = $this->httpClient->request('POST', $this->configuration['endpoint'], [
                    'body'   => [
                        'grant_type' => 'refresh_token',
                        'client_id' => $this->configuration['client_id'],
                        'client_secret' => $this->configuration['client_secret'],
                        'refresh_token' => $oAuthToken->getRefreshToken(),
                    ],
                ]);
                $data = $response->getContent();
                $serializer = new Serializer([new ObjectNormalizer()], [new JsonEncoder()]);
                $oAuthToken = $serializer->deserialize($data, OAuthToken::class, 'json');
            } catch (ExceptionInterface $exception) {
                $oAuthToken = false;
            }
        }
        // At this stage, user is either authenticated or need to login again.
        if (!$this->checkCredentials($oAuthToken, $user) || $oAuthToken->isExpired()) {
            $oAuthToken = false;
        }
        return $oAuthToken;
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        /** @var OAuthUserInterface $user */
        $user = $this->security->getUser();
        if (!$user->getOAuthToken()->isExpired()) {
            try {
                /** @var UserInterface $userEntity */
                $userEntity = $this->entityFactory->query(User::class)
                    ->addFilter(new Filter('name', '=', $user->getUsername()))
                    ->addIncludePath(new IncludePath('roles'))
                    ->load();
            } catch (DrupalJsonApiException $exception) {
                return null;
            }
            $user->setUserEntity($userEntity);
        }
        if ($credentials instanceof OAuthTokenInterface) {
            $user->setOAuthToken($credentials);
        }
        return $user;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $loginPath = $this->router->generate($this->configuration['interactive_login']['login_route']);
        if ($request->getPathInfo() !== $loginPath) {
            $response = new RedirectResponse($loginPath);
            $response->headers->removeCookie('PHPSESSID');
            $response->headers->removeCookie('REMEMBERME');
            $response->send();
        }
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        // No actions on success.
        return null;
    }

    public function start(Request $request, AuthenticationException $exception = null)
    {
        return new RedirectResponse($this->router->generate($this->configuration['interactive_login']['login_redirect']));
    }

    public function supports(Request $request)
    {
        /** @var OAuthUserInterface $user */
        $user = $this->security->getUser();
        return is_a($user, OAuthUserInterface::class) && (
            $user->getOAuthToken()->isExpired() ||
            $user->getUserEntity()->getUuid() === null
        );
    }

    public function supportsRememberMe()
    {
        return false;
    }
}
