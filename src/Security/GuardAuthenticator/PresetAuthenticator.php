<?php

namespace Roensby\SymfonyDrupalJsonApi\Security\GuardAuthenticator;

use Roensby\SymfonyDrupalJsonApi\Constant;
use Roensby\SymfonyDrupalJsonApi\Entity\Core\User;
use Roensby\SymfonyDrupalJsonApi\Factory\EntityFactory;
use Roensby\SymfonyDrupalJsonApi\Security\OAuthToken\OAuthToken;
use Roensby\SymfonyDrupalJsonApi\Security\OAuthToken\OAuthTokenInterface;
use Roensby\SymfonyDrupalJsonApi\Security\User\OAuthUser;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Component\Security\Http\Util\TargetPathTrait;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Validator\Validation;
use Symfony\Contracts\HttpClient\Exception\ExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * This class provides a preset user, useful for non-interactive services
 * that needs to work with Drupal entities.
 * For more information and instructions for use, see
 * https://symfony.com/doc/current/security/guard_authentication.html
 */
class PresetAuthenticator extends AbstractGuardAuthenticator
{
    use TargetPathTrait;

    /**
     * @var array
     */
    protected $configuration;

    /**
     * @var HttpClientInterface
     */
    protected $httpClient;

    /**
     * @var CsrfTokenManagerInterface
     */
    protected $csrfTokenManager;

    /**
     * @var EntityFactory
     */
    protected $entityFactory;

    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * @var Security
     */
    protected $security;

    public function __construct(
        array $configuration,
        CsrfTokenManagerInterface $csrfTokenManager,
        EntityFactory $entityFactory,
        HttpClientInterface $httpClient,
        RouterInterface $router,
        Security $security
    ) {
        $this->configuration = $configuration;
        $this->csrfTokenManager = $csrfTokenManager;
        $this->entityFactory = $entityFactory;
        $this->httpClient = $httpClient;
        $this->router = $router;
        $this->security = $security;
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        if (!is_a($credentials, OAuthTokenInterface::class)) {
            return false;
        }
        $validator = Validation::createValidatorBuilder()
            ->addMethodMapping('loadValidatorMetadata')
            ->getValidator();
        $violations = $validator->validate($credentials);
        return count($violations) === 0;
    }

    public function getCredentials(Request $request)
    {
        try {
            $response = $this->httpClient->request('POST', $this->configuration['endpoint'], [
                'body' => [
                    'grant_type' => 'password',
                    'client_id' => $this->configuration['client_id'],
                    'client_secret' => $this->configuration['client_secret'],
                    'username' => $this->configuration['preset_user']['username'],
                    'password' => $this->configuration['preset_user']['password'],
                ],
            ]);
            $data = $response->getContent();
            $serializer = new Serializer([new ObjectNormalizer()], [new JsonEncoder()]);
            /** @var OAuthTokenInterface $oAuthToken */
            $oAuthToken = $serializer->deserialize($data, OAuthToken::class, 'json');
        } catch (ExceptionInterface $exception) {
            throw new CustomUserMessageAuthenticationException($exception->getMessage());
        }
        return $oAuthToken;
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $user = new OAuthUser();
        $user->setOAuthToken($credentials);
        $userEntity = new User();
        $userEntity->setName($this->configuration['preset_user']['username']);
        $user->setUserEntity($userEntity);
        return $user;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        throw new BadRequestHttpException('Authentication failure.');
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        return null;
    }

    public function start(Request $request, AuthenticationException $exception = null)
    {
        return null;
    }

    public function supports(Request $request)
    {
        /** @var OAuthTokenInterface $user */
        $user = $this->security->getUser();
        return $user === null;
    }

    public function supportsRememberMe()
    {
        return false;
    }
}
