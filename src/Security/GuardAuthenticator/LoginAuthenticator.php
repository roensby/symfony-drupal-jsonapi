<?php

namespace Roensby\SymfonyDrupalJsonApi\Security\GuardAuthenticator;

use Roensby\SymfonyDrupalJsonApi\Entity\Core\User;
use Roensby\SymfonyDrupalJsonApi\Factory\EntityFactory;
use Roensby\SymfonyDrupalJsonApi\Security\OAuthToken\OAuthToken;
use Roensby\SymfonyDrupalJsonApi\Security\OAuthToken\OAuthTokenInterface;
use Roensby\SymfonyDrupalJsonApi\Security\User\OAuthUser;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;
use Symfony\Component\Security\Http\Util\TargetPathTrait;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\HttpClient\Exception\ExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * This class provides login support for interactive login forms.
 * For more information and instructions for use, see
 * https://symfony.com/doc/current/security/guard_authentication.html
 */
class LoginAuthenticator extends AbstractFormLoginAuthenticator
{
    use TargetPathTrait;

    /**
     * @var array
     */
    protected $configuration;

    /**
     * @var HttpClientInterface
     */
    protected $httpClient;

    /**
     * @var CsrfTokenManagerInterface
     */
    protected $csrfTokenManager;

    /**
     * @var EntityFactory
     */
    protected $entityFactory;

    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * @var UserPasswordEncoderInterface
     */
    protected $passwordEncoder;

    /**
     * @var ValidatorInterface
     */
    protected $validator;

    public function __construct(
        array $configuration,
        CsrfTokenManagerInterface $csrfTokenManager,
        EntityFactory $entityFactory,
        HttpClientInterface $httpClient,
        RouterInterface $router,
        UserPasswordEncoderInterface $passwordEncoder,
        ValidatorInterface $validator
    ) {
        $this->configuration = $configuration;
        $this->csrfTokenManager = $csrfTokenManager;
        $this->entityFactory = $entityFactory;
        $this->httpClient = $httpClient;
        $this->router = $router;
        $this->passwordEncoder = $passwordEncoder;
        $this->validator = $validator;
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        return $this->passwordEncoder->isPasswordValid($user, $credentials['password']);
    }

    public function getCredentials(Request $request)
    {
        $credentials = [
            'username' => $request->request->get('username'),
            'password' => $request->request->get('password'),
            'csrf_token' => $request->request->get('_csrf_token'),
        ];
        $request->getSession()->set(Security::LAST_USERNAME, $credentials['username']);
        return $credentials;
    }

    protected function getLoginUrl()
    {
        return $this->router->generate($this->configuration['interactive_login']['login_route']);
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $token = new CsrfToken('authenticate', $credentials['csrf_token']);
        if (!$this->csrfTokenManager->isTokenValid($token)) {
            throw new InvalidCsrfTokenException();
        }
        try {
            $user = new OAuthUser();
            $user->setPassword($this->passwordEncoder->encodePassword($user, $credentials['password']));
            $response = $this->httpClient->request('POST', $this->configuration['endpoint'], [
                'body'   => [
                    'grant_type' => 'password',
                    'client_id' => $this->configuration['client_id'],
                    'client_secret' => $this->configuration['client_secret'],
                    'username' => $credentials['username'],
                    'password' => $credentials['password'],
                ],
            ]);
            $data = $response->getContent();
            $serializer = new Serializer([new ObjectNormalizer()], [new JsonEncoder()]);
            /** @var OAuthTokenInterface $oAuthToken */
            $oAuthToken = $serializer->deserialize($data, OAuthToken::class, 'json');
            // Validate oAuthToken.
            if (count($this->validator->validate($oAuthToken)) > 0) {
                throw new CustomUserMessageAuthenticationException('Failed to authenticate. Please try again.');
            }
            $user->setOAuthToken($oAuthToken);
            $userEntity = new User();
            $userEntity->setName($credentials['username']);
            $user->setUserEntity($userEntity);
            return $user;
        } catch (ExceptionInterface $exception) {
            throw new CustomUserMessageAuthenticationException('Failed to authenticate. Please try again.');
        }
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        $response = null;
        if ($targetPath = $this->getTargetPath($request->getSession(), $providerKey)) {
            $response = new RedirectResponse($targetPath);
        } else {
            $response = new RedirectResponse($this->router->generate($this->configuration['interactive_login']['login_redirect']));
        }
        return $response;
    }

    public function supports(Request $request)
    {
        return $this->configuration['interactive_login']['login_route'] === $request->attributes->get('_route')
            && $request->isMethod('POST');
    }
}
