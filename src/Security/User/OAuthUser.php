<?php

namespace Roensby\SymfonyDrupalJsonApi\Security\User;

use Roensby\SymfonyDrupalJsonApi\Constant;
use Roensby\SymfonyDrupalJsonApi\Entity\Core\UserInterface;
use Roensby\SymfonyDrupalJsonApi\Entity\Core\UserRoleInterface;
use Roensby\SymfonyDrupalJsonApi\Security\OAuthToken\OAuthTokenInterface;

class OAuthUser implements OAuthUserInterface
{
    /**
     * @var string The hashed password
     */
    protected $password;

    /**
     * @var OAuthTokenInterface
     */
    protected $oAuthToken;

    /**
     * @var UserInterface|null
     */
    protected $userEntity;


    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * Getters.
     */

    public function getOAuthToken(): OAuthTokenInterface
    {
        return $this->oAuthToken;
    }

    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function getRoles(): array
    {
        $roles = [];
        if (!empty($this->userEntity)) {
            /** @var UserRoleInterface $role */
            foreach ($this->userEntity->getRoles() as $role) {
                $roles[] = sprintf('%s%s', Constant::SYMFONY_ROLE_PREFIX, strtoupper($role->getId()));
            }
        }
        return $roles;
    }

    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    public function getUserEntity(): ?UserInterface
    {
        return $this->userEntity;
    }

    public function getUsername(): ?string
    {
        return !empty($this->userEntity) ? $this->userEntity->getName() : null;
    }

    /**
     * Setters.
     */

    public function setOAuthToken(OAuthTokenInterface $oAuthToken): OAuthUserInterface
    {
        $this->oAuthToken = $oAuthToken;
        return $this;
    }

    public function setPassword(string $clear_text_password): OAuthUserInterface
    {
        $this->password = $clear_text_password;
        return $this;
    }

    public function setUserEntity(UserInterface $user): OAuthUserInterface
    {
        $this->userEntity = $user;
        return $this;
    }
}
