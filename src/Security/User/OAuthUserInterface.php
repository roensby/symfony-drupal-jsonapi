<?php

namespace Roensby\SymfonyDrupalJsonApi\Security\User;

use Roensby\SymfonyDrupalJsonApi\Entity\Core\UserInterface;
use Roensby\SymfonyDrupalJsonApi\Security\OAuthToken\OAuthTokenInterface;
use Symfony\Component\Security\Core\User\UserInterface as SymfonyUserInterface;

interface OAuthUserInterface extends SymfonyUserInterface
{
    /**
     * Getters.
     */

    public function getOAuthToken(): OAuthTokenInterface;

    public function getUserEntity(): ?UserInterface;

    /**
     * Setters.
     */

    public function setOAuthToken(OAuthTokenInterface $oAuthToken): OAuthUserInterface;

    public function setUserEntity(UserInterface $user): OAuthUserInterface;
}
