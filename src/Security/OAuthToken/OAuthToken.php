<?php

namespace Roensby\SymfonyDrupalJsonApi\Security\OAuthToken;

use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Mapping\ClassMetadata;

class OAuthToken implements OAuthTokenInterface
{
    const TIMEOUT_BUFFER = 3600;

    /**
     * @var ?string
     */
    protected $accessToken;

    /**
     * @var ?int
     */
    protected $expiresIn;

    /**
     * @var ?string
     */
    protected $refreshToken;

    /**
     * Getters.
     */

    public function getAccessToken(): ?string
    {
        return $this->accessToken;
    }

    public function getExpiresIn(): ?int
    {
        return $this->expiresIn;
    }

    public function getRefreshToken(): ?string
    {
        return $this->refreshToken;
    }

    public function isExpired(): bool
    {
        return !empty($this->expiresIn) ? time() > $this->expiresIn - self::TIMEOUT_BUFFER : true;
    }

    /**
     * Validation.
     */

    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('accessToken', new NotBlank());
        $metadata->addPropertyConstraint('expiresIn', new NotBlank());
        $metadata->addPropertyConstraint('refreshToken', new NotBlank());
    }

    /**
     * Setters.
     */

    public function setAccessToken(string $accessToken): OAuthTokenInterface
    {
        $this->accessToken = $accessToken;
        return $this;
    }

    public function setExpiresIn(int $expiresIn): OAuthTokenInterface
    {
        $this->expiresIn = time() + $expiresIn;
        return $this;
    }

    public function setRefreshToken(string $refreshToken): OAuthTokenInterface
    {
        $this->refreshToken = $refreshToken;
        return $this;
    }
}
