<?php

namespace Roensby\SymfonyDrupalJsonApi\Security\OAuthToken;

interface OAuthTokenInterface
{
    /**
     * Getters.
     */
    public function getAccessToken(): ?string;

    public function getExpiresIn(): ?int;

    public function getRefreshToken(): ?string;

    public function isExpired(): bool;

    /**
     * Setters.
     */
    public function setAccessToken(string $accessToken): OAuthTokenInterface;

    public function setExpiresIn(int $expiresIn): OAuthTokenInterface;

    public function setRefreshToken(string $refreshToken): OAuthTokenInterface;
}
