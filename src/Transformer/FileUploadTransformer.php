<?php declare(strict_types = 1);

namespace Roensby\SymfonyDrupalJsonApi\Transformer;

use Roensby\SymfonyDrupalJsonApi\Entity\Base\EntityBaseInterface;
use Roensby\SymfonyDrupalJsonApi\Exception\CsrfTokenException;
use Roensby\SymfonyDrupalJsonApi\Exception\EntityException;
use Roensby\SymfonyDrupalJsonApi\Exception\EntityMalformedException;
use Roensby\SymfonyDrupalJsonApi\Exception\EntityNotFoundException;
use Roensby\SymfonyDrupalJsonApi\Storage\FileStorage;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploadTransformer implements FileUploadTransformerInterface
{
    /**
     * @var FileStorage
     */
    protected $fileStorage;

    /**
     * @var EntityBaseInterface
     */
    protected $parentEntity;

    /**
     * @var string
     */
    protected $parentFieldName;

    public function __construct(FileStorage $fileStorage)
    {
        $this->fileStorage = $fileStorage;
    }

    public function reverseTransform($uploadedFile)
    {
        $file = null;
        try {
            if (
                !empty($this->parentEntity) &&
                !empty($this->parentFieldName) &&
                $uploadedFile !== null &&
                get_class($uploadedFile) === UploadedFile::class
            ) {
                $file = $this->fileStorage->create($uploadedFile, $this->parentEntity, $this->parentFieldName);
            }
        } catch (CsrfTokenException $exception) {
            dump($exception);
        } catch (EntityNotFoundException $exception) {
            dump($exception);
        } catch (EntityMalformedException $exception) {
            dump($exception);
        } catch (EntityException $exception) {
            dump($exception);
        }
        return $file;
    }

    public function setParentEntity(EntityBaseInterface $parentEntity): FileUploadTransformerInterface
    {
        $this->parentEntity = $parentEntity;
        return $this;
    }

    public function setParentFieldName(string $parentFieldName): FileUploadTransformerInterface
    {
        $this->parentFieldName = $parentFieldName;
        return $this;
    }

    public function transform($file)
    {
        return $file === null ? null : new UploadedFile($file->getFilename(), $file->getFilename(), $file->getFilemime(), UPLOAD_ERR_NO_FILE);
    }
}
