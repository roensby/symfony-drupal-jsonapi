<?php

namespace Roensby\SymfonyDrupalJsonApi\Transformer;

use Roensby\SymfonyDrupalJsonApi\Entity\Base\EntityBaseInterface;
use Symfony\Component\Form\DataTransformerInterface;

interface FileUploadTransformerInterface extends DataTransformerInterface
{
    public function setParentEntity(EntityBaseInterface $parentEntity): FileUploadTransformerInterface;

    public function setParentFieldName(string $parentFieldName): FileUploadTransformerInterface;
}
