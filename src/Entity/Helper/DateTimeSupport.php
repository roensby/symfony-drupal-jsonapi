<?php

namespace Roensby\SymfonyDrupalJsonApi\Entity\Helper;

use DateTime;
use Roensby\SymfonyDrupalJsonApi\Constant;

trait DateTimeSupport
{
    private function getDateTime($value): ?DateTime
    {
        if (is_string($value)) {
            $datetime = DateTime::createFromFormat(Constant::JSONAPI_ISO_8601_DATETIME_FORMAT, $value);
            if ($datetime !== false && $datetime->format(Constant::JSONAPI_ISO_8601_DATETIME_FORMAT) === $value) {
                return $datetime;
            }
        } elseif (get_class($value) === DateTime::class) {
            return $value;
        }
        return null;
    }
}
