<?php

namespace Roensby\SymfonyDrupalJsonApi\Entity\Core;

use DateTime;
use Roensby\SymfonyDrupalJsonApi\Entity\Base\ContentEntityBaseInterface;

interface UserInterface extends ContentEntityBaseInterface
{
    /**
     * Getters.
     */

    public function getAccess(): ?DateTime;

    public function getChanged(): ?DateTime;

    public function getCreated(): ?DateTime;

    public function getInit(): ?string;

    public function getLogin(): ?DateTime;

    public function getMail(): ?string;

    public function getName(): ?string;

    public function getRoles(): array;

    public function getStatus(): ?bool;

    public function getTimezone(): ?string;

    /**
     * Setters.
     */

    public function setAccess($access): UserInterface;

    public function setChanged($changed): UserInterface;

    public function setCreated($created): UserInterface;

    public function setInit(?string $init): UserInterface;

    public function setLogin($login): UserInterface;

    public function setMail(?string $mail): UserInterface;

    public function setName(?string $name): UserInterface;

    public function setRoles(array $roles): UserInterface;

    public function setStatus(?bool $status): UserInterface;

    public function setTimezone(?string $timezone): UserInterface;
}
