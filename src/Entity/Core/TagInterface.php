<?php

namespace Roensby\SymfonyDrupalJsonApi\Entity\Core;

use Roensby\SymfonyDrupalJsonApi\Entity\Core\Bundle\TaxonomyTermInterface;

interface TagInterface extends TaxonomyTermInterface
{
}
