<?php

namespace Roensby\SymfonyDrupalJsonApi\Entity\Core;

use DateTime;
use Roensby\SymfonyDrupalJsonApi\Entity\Base\ContentEntityBaseInterface;

interface FileInterface extends ContentEntityBaseInterface
{
    /**
     * Getters.
     */

    public function getAlt(): ?string;

    public function getChanged(): ?DateTime;

    public function getCreated(): ?DateTime;

    public function getFilemime(): ?string;

    public function getFilename(): ?string;

    public function getFilesize(): ?int;

    public function getHeight(): ?int;

    public function getStatus(): ?bool;

    public function getTitle(): ?string;

    public function getUri(): array;

    public function getWidth(): ?int;

    /**
     * Setters.
     */

    public function setAlt(?string $alt): FileInterface;

    public function setChanged($changed): FileInterface;

    public function setCreated($created): FileInterface;

    public function setFilemime(?string $filemime): FileInterface;

    public function setFilename(?string $filename): FileInterface;

    public function setFilesize(?int $filesize): FileInterface;

    public function setHeight(?int $height): FileInterface;

    public function setStatus(?bool $status): FileInterface;

    public function setTitle(?string $title): FileInterface;

    public function setUri(array $uri): FileInterface;

    public function setWidth(?int $width): FileInterface;
}
