<?php declare(strict_types = 1);

namespace Roensby\SymfonyDrupalJsonApi\Entity\Core;

use Roensby\SymfonyDrupalJsonApi\Entity\Core\Bundle\Node;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

class Article extends Node implements ArticleInterface
{
    /**
     * @Groups("attributes")
     * @var string[]
     */
    protected $body = [];

    /**
     * @var string
     */
    protected $bundle = 'article';

    /**
     * @Groups("relationships")
     * @SerializedName("field_image")
     * @var ?File
     */
    protected $image;

    /**
     * @Groups("relationships")
     * @SerializedName("field_tags")
     * @var Tag[]
     */
    protected $tags = [];

    /**
     * Getters.
     */

    public function getBody(): array
    {
        return $this->body;
    }

    public function getBodyValue(): ?string
    {
        return isset($this->body['value']) ? $this->body['value'] : null;
    }

    public function getImage(): ?FileInterface
    {
        return $this->image;
    }

    public function getTags(): array
    {
        return $this->tags;
    }

    /**
     * Setters.
     */

    public function setBody(array $body): ArticleInterface
    {
        $this->body = $body;
        return $this;
    }

    public function setBodyValue(?string $value): ArticleInterface
    {
        $this->body['value'] = $value;
        return $this;
    }

    public function setImage(?FileInterface $image): ArticleInterface
    {
        $this->image = $image;
        return $this;
    }

    public function setTags(array $tags): ArticleInterface
    {
        $this->tags = $tags;
        return $this;
    }
}
