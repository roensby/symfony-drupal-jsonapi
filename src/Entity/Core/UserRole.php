<?php declare(strict_types = 1);

namespace Roensby\SymfonyDrupalJsonApi\Entity\Core;

use Roensby\SymfonyDrupalJsonApi\Constant;
use Roensby\SymfonyDrupalJsonApi\Entity\Base\ConfigEntityBase;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Mapping\ClassMetadata;

class UserRole extends ConfigEntityBase implements UserRoleInterface
{
    /**
     * @Groups("attributes")
     * @var ?bool
     */
    protected $isAdmin;

    /**
     * @var string
     */
    protected $bundle = Constant::ENTITY_USER_ROLE;

    /**
     * @Groups("attributes")
     * @var string[]
     */
    protected $permissions = [];

    /**
     * @var string
     */
    protected $type = Constant::ENTITY_USER_ROLE;

    /**
     * Getters.
     */

    public function getIsAdmin(): ?bool
    {
        return $this->isAdmin;
    }

    public function getPermissions(): array
    {
        return $this->permissions;
    }

    /**
     * Validation.
     *
     * @param ClassMetadata $metadata
     */
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('id', new NotBlank());
        $metadata->addPropertyConstraint('uuid', new NotBlank());
    }

    /**
     * Setters.
     */

    public function setIsAdmin(?bool $isAdmin): UserRoleInterface
    {
        $this->isAdmin = $isAdmin;
        return $this;
    }

    public function setPermissions(array $permissions): UserRoleInterface
    {
        $this->permissions = $permissions;
        return $this;
    }
}
