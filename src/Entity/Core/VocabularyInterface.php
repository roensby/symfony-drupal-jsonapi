<?php

namespace Roensby\SymfonyDrupalJsonApi\Entity\Core;

interface VocabularyInterface
{
    /**
     * Getters.
     */

    public function getDescription(): ?string;

    public function getName(): ?string;

    public function getWeight(): ?int;

    /**
     * Setters.
     */

    public function setDescription(?string $description): VocabularyInterface;

    public function setName(?string $name): VocabularyInterface;

    public function setWeight(?int $weight): VocabularyInterface;
}
