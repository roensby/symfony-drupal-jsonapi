<?php declare(strict_types = 1);

namespace Roensby\SymfonyDrupalJsonApi\Entity\Core;

use DateTime;
use Roensby\SymfonyDrupalJsonApi\Constant;
use Roensby\SymfonyDrupalJsonApi\Entity\Base\ContentEntityBase;
use Roensby\SymfonyDrupalJsonApi\Entity\Helper\DateTimeSupport;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Mapping\ClassMetadata;

class File extends ContentEntityBase implements FileInterface
{
    use DateTimeSupport;

    /**
     * @Groups("meta")
     * @var ?string
     */
    protected $alt;

    /**
     * @var string
     */
    protected $bundle = Constant::BUNDLE_FILE;

    /**
     * @Groups("attributes")
     * @var ?DateTime
     */
    protected $changed;

    /**
     * @Groups("attributes")
     * @var ?DateTime
     */
    protected $created;

    /**
     * @Groups("attributes")
     * @var ?string
     */
    protected $filemime;

    /**
     * @Groups("attributes")
     * @var ?string
     */
    protected $filename;

    /**
     * @Groups("attributes")
     * @var ?int
     */
    protected $filesize;

    /**
     * @Groups("meta")
     * @var ?int
     */
    protected $height;

    /**
     * @Groups("attributes")
     * @SerializedName("drupal_internal__fid")
     * @var ?int
     */
    protected $id;

    /**
     * @Groups("attributes")
     * @var ?bool
     */
    protected $status;

    /**
     * @Groups("meta")
     * @var ?string
     */
    protected $title;

    /**
     * @var string
     */
    protected $type = Constant::ENTITY_FILE;

    /**
     * @Groups("attributes")
     * @var array
     */
    protected $uri = [];

    /**
     * @Groups("meta")
     * @var ?int
     */
    protected $width;

    /**
     * Getters.
     */

    public function getAlt(): ?string
    {
        return $this->alt;
    }

    public function getChanged(): ?DateTime
    {
        return $this->changed;
    }

    public function getCreated(): ?DateTime
    {
        return $this->created;
    }

    public function getFilemime(): ?string
    {
        return $this->filemime;
    }

    public function getFilename(): ?string
    {
        return $this->filename;
    }

    public function getFilesize(): ?int
    {
        return $this->filesize;
    }

    public function getHeight(): ?int
    {
        return $this->height;
    }

    public function getStatus(): ?bool
    {
        return $this->status;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function getUri(): array
    {
        return $this->uri;
    }

    public function getWidth(): ?int
    {
        return $this->width;
    }

    /**
     * Validation.
     */

    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('filename', new NotBlank());
        $metadata->addPropertyConstraint('status', new NotBlank());
        $metadata->addPropertyConstraint('uri', new NotBlank());
    }

    /**
     * Setters.
     */

    public function setAlt(?string $alt): FileInterface
    {
        $this->alt = $alt;
        return $this;
    }

    public function setChanged($changed): FileInterface
    {
        $this->changed = $this->getDateTime($changed);
        return $this;
    }


    public function setCreated($created): FileInterface
    {
        $this->created = $this->getDateTime($created);
        return $this;
    }

    public function setFilemime(?string $filemime): FileInterface
    {
        $this->filemime = $filemime;
        return $this;
    }

    public function setFilename(?string $filename): FileInterface
    {
        $this->filename = $filename;
        return $this;
    }

    public function setFilesize(?int $filesize): FileInterface
    {
        $this->filesize = $filesize;
        return $this;
    }

    public function setHeight(?int $height): FileInterface
    {
        $this->height = $height;
        return $this;
    }

    public function setStatus(?bool $status): FileInterface
    {
        $this->status = $status;
        return $this;
    }

    public function setTitle(?string $title): FileInterface
    {
        $this->title = $title;
        return $this;
    }

    public function setUri(array $uri): FileInterface
    {
        $this->uri = $uri;
        return $this;
    }

    public function setWidth(?int $width): FileInterface
    {
        $this->width = $width;
        return $this;
    }
}
