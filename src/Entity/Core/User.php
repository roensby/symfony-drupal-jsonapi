<?php declare(strict_types = 1);

namespace Roensby\SymfonyDrupalJsonApi\Entity\Core;

use DateTime;
use Roensby\SymfonyDrupalJsonApi\Constant;
use Roensby\SymfonyDrupalJsonApi\Entity\Base\ContentEntityBase;
use Roensby\SymfonyDrupalJsonApi\Entity\Helper\DateTimeSupport;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Mapping\ClassMetadata;

class User extends ContentEntityBase implements UserInterface
{
    use DateTimeSupport;

    /**
     * @Groups("attributes")
     * @var ?DateTime
     */
    protected $access;

    /**
     * @var string
     */
    protected $bundle = Constant::ENTITY_USER;

    /**
     * @Groups("attributes")
     * @var ?DateTime
     */
    protected $changed;

    /**
     * @Groups("attributes")
     * @var ?DateTime
     */
    protected $created;

    /**
     * @Groups("attributes")
     * @SerializedName("drupal_internal__uid")
     * @var ?int
     */
    protected $id;

    /**
     * @Groups("attributes")
     * @var ?string
     */
    protected $init;

    /**
     * @Groups("attributes")
     * @var ?DateTime
     */
    protected $login;

    /**
     * @Groups("attributes")
     * @var ?string
     */
    protected $mail;

    /**
     * @Groups("attributes")
     * @var ?string
     */
    protected $name;

    /**
     * @Groups("attributes")
     * @var ?string
     */
    protected $preferredAdminLangcode;

    /**
     * @Groups("attributes")
     * @var ?string
     */
    protected $preferredLangcode;

    /**
     * @Groups("relationships")
     * @var UserRole[]
     */
    protected $roles = [];

    /**
     * @Groups("attributes")
     * @var ?bool
     */
    protected $status;

    /**
     * @Groups("attributes")
     * @var ?string
     */
    protected $timezone;

    /**
     * @var string
     */
    protected $type = Constant::ENTITY_USER;

    /**
     * Getters.
     */

    public function getAccess(): ?DateTime
    {
        return $this->access;
    }

    public function getChanged(): ?DateTime
    {
        return $this->changed;
    }

    public function getCreated(): ?DateTime
    {
        return $this->created;
    }

    public function getInit(): ?string
    {
        return $this->init;
    }

    public function getLogin(): ?DateTime
    {
        return $this->login;
    }

    public function getMail(): ?string
    {
        return $this->mail;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getRoles(): array
    {
        return $this->roles;
    }

    public function getStatus(): ?bool
    {
        return $this->status;
    }

    public function getTimezone(): ?string
    {
        return $this->timezone;
    }

    /**
     * Validation.
     *
     * @param ClassMetadata $metadata
     */
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('mail', new NotBlank());
        $metadata->addPropertyConstraint('name', new NotBlank());
    }

    /**
     * Setters.
     */

    public function setAccess($access): UserInterface
    {
        $this->access = $this->getDateTime($access);
        return $this;
    }

    public function setChanged($changed): UserInterface
    {
        $this->changed = $this->getDateTime($changed);
        return $this;
    }

    public function setCreated($created): UserInterface
    {
        $this->created = $this->getDateTime($created);
        return $this;
    }

    public function setInit(?string $init): UserInterface
    {
        $this->init = $init;
        return $this;
    }

    public function setLogin($login): UserInterface
    {
        $this->login = $this->getDateTime($login);
        return $this;
    }

    public function setMail(?string $mail): UserInterface
    {
        $this->mail = $mail;
        return $this;
    }

    public function setName(?string $name): UserInterface
    {
        $this->name = $name;
        return $this;
    }

    public function setRoles(array $roles): UserInterface
    {
        $this->roles = $roles;
        return $this;
    }

    public function setStatus(?bool $status): UserInterface
    {
        $this->status = $status;
        return $this;
    }

    public function setTimezone(?string $timezone): UserInterface
    {
        $this->timezone = $timezone;
        return $this;
    }
}
