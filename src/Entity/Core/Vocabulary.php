<?php declare(strict_types = 1);

namespace Roensby\SymfonyDrupalJsonApi\Entity\Core;

use Roensby\SymfonyDrupalJsonApi\Constant;
use Roensby\SymfonyDrupalJsonApi\Entity\Base\ConfigEntityBase;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Mapping\ClassMetadata;

class Vocabulary extends ConfigEntityBase implements VocabularyInterface
{
    /**
     * @var string
     */
    protected $bundle = Constant::ENTITY_VOCABULARY;

    /**
     * @Groups("attributes")
     * @SerializedName("drupal_internal__vid")
     * @var ?string
     */
    protected $id;

    /**
     * @Groups("attributes")
     * @SerializedName("name")
     * @var ?string
     */
    protected $label;

    /**
     * @Groups("attributes")
     * @var ?string
     */
    protected $description;

    /**
     * @var string
     */
    protected $type = Constant::ENTITY_VOCABULARY;

    /**
     * @Groups("attributes")
     * @var ?int
     */
    protected $weight;

    /**
     * Getters.
     */

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getName(): ?string
    {
        return $this->label;
    }

    public function getWeight(): ?int
    {
        return $this->weight;
    }

    /**
     * Validation.
     *
     * @param ClassMetadata $metadata
     */
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('id', new NotBlank());
        $metadata->addPropertyConstraint('uuid', new NotBlank());
    }

    /**
     * Setters.
     */

    public function setDescription(?string $description): VocabularyInterface
    {
        $this->description = $description;
        return $this;
    }

    public function setName(?string $name): VocabularyInterface
    {
        $this->label = $name;
        return $this;
    }

    public function setWeight(?int $weight): VocabularyInterface
    {
        $this->weight = $weight;
        return $this;
    }
}
