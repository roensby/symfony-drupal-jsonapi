<?php declare(strict_types = 1);

namespace Roensby\SymfonyDrupalJsonApi\Entity\Core;

use Roensby\SymfonyDrupalJsonApi\Constant;
use Roensby\SymfonyDrupalJsonApi\Entity\Core\Bundle\TaxonomyTerm;

class Tag extends TaxonomyTerm implements TagInterface
{
    /**
     * @var string
     */
    protected $bundle = Constant::BUNDLE_TAGS;
}
