<?php

namespace Roensby\SymfonyDrupalJsonApi\Entity\Core\Bundle;

use DateTime;
use Roensby\SymfonyDrupalJsonApi\Entity\Base\EditorialContentEntityBaseInterface;
use Roensby\SymfonyDrupalJsonApi\Entity\Core\FileInterface;
use Roensby\SymfonyDrupalJsonApi\Entity\Core\UserInterface;

interface MediaInterface extends EditorialContentEntityBaseInterface
{
    /**
     * Getters.
     */
    public function getChanged(): ?DateTime;

    public function getCreated(): ?DateTime;

    public function getName(): ?string;

    public function getThumbnail(): ?FileInterface;

    public function getType(): string;

    public function getUid(): ?UserInterface;

    /**
     * Setters.
     */
    public function setChanged($changed): MediaInterface;

    public function setCreated($created): MediaInterface;

    public function setName(?string $name): MediaInterface;

    public function setThumbnail(?FileInterface $thumbnail): MediaInterface;

    public function setType(string $type): MediaInterface;

    public function setUid(?UserInterface $uid): MediaInterface;
}
