<?php

namespace Roensby\SymfonyDrupalJsonApi\Entity\Core\Bundle;

use Roensby\SymfonyDrupalJsonApi\Entity\Base\EntityBaseInterface;

interface TaxonomyTermInterface extends EntityBaseInterface
{
    /**
     * Getters.
     */

    public function getDescription(): array;

    public function getDescriptionValue(): ?string;

    public function getName(): ?string;

    public function getParent(): array;

    /**
     * Setters.
     */

    public function setDescription(?array $description): TaxonomyTermInterface;

    public function setDescriptionValue(?string $description): TaxonomyTermInterface;

    public function setName(?string $name): TaxonomyTermInterface;

    public function setParent(array $parent): TaxonomyTermInterface;
}
