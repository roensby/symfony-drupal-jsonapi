<?php declare(strict_types = 1);

namespace Roensby\SymfonyDrupalJsonApi\Entity\Core\Bundle;

use DateTime;
use Roensby\SymfonyDrupalJsonApi\Constant;
use Roensby\SymfonyDrupalJsonApi\Entity\Base\EditorialContentEntityBase;
use Roensby\SymfonyDrupalJsonApi\Entity\Core\File;
use Roensby\SymfonyDrupalJsonApi\Entity\Core\FileInterface;
use Roensby\SymfonyDrupalJsonApi\Entity\Core\User;
use Roensby\SymfonyDrupalJsonApi\Entity\Core\UserInterface;
use Roensby\SymfonyDrupalJsonApi\Entity\Helper\DateTimeSupport;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Mapping\ClassMetadata;

abstract class Media extends EditorialContentEntityBase implements MediaInterface
{
    use DateTimeSupport;

    /**
     * @Groups("attributes")
     * @var ?DateTime
     */
    protected $changed;

    /**
     * @Groups("attributes")
     * @var ?DateTime
     */
    protected $created;

    /**
     * @Groups("attributes")
     * @SerializedName("drupal_internal__mid")
     * @var int
     */
    protected $id;

    /**
     * @Groups("attributes")
     * @var ?string
     */
    protected $name;

    /**
     * @Groups("relationships")
     * @var ?File
     */
    protected $thumbnail;

    /**
     * @var string
     */
    protected $type = Constant::ENTITY_MEDIA;

    /**
     * @Groups("relationships")
     * @var ?User
     */
    protected $uid;

    /**
     * Getters.
     */

    public function getChanged(): ?DateTime
    {
        return $this->changed;
    }

    public function getCreated(): ?DateTime
    {
        return $this->created;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getThumbnail(): ?FileInterface
    {
        return $this->thumbnail;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getUid(): ?UserInterface
    {
        return $this->uid;
    }

    /**
     * Validation.
     *
     * @param ClassMetadata $metadata
     */
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        parent::loadValidatorMetadata($metadata);
    }

    /**
     * Setters.
     */

    public function setChanged($changed): MediaInterface
    {
        $this->changed = $this->getDateTime($changed);
        return $this;
    }

    public function setCreated($created): MediaInterface
    {
        $this->created = $this->getDateTime($created);
        return $this;
    }

    public function setName(?string $name): MediaInterface
    {
        $this->name = $name;
        return $this;
    }

    public function setThumbnail(?FileInterface $thumbnail): MediaInterface
    {
        $this->thumbnail = $thumbnail;
        return $this;
    }

    public function setType(string $type): MediaInterface
    {
        $this->type = $type;
        return $this;
    }

    public function setUid(?UserInterface $uid): MediaInterface
    {
        $this->uid = $uid;
        return $this;
    }
}
