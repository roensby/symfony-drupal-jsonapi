<?php declare(strict_types = 1);

namespace Roensby\SymfonyDrupalJsonApi\Entity\Core\Bundle;

use DateTime;
use Roensby\SymfonyDrupalJsonApi\Constant;
use Roensby\SymfonyDrupalJsonApi\Entity\Base\ContentEntityBase;
use Roensby\SymfonyDrupalJsonApi\Entity\Base\EntityBase;
use Roensby\SymfonyDrupalJsonApi\Entity\Base\EntityBaseInterface;
use Roensby\SymfonyDrupalJsonApi\Entity\Core\User;
use Roensby\SymfonyDrupalJsonApi\Entity\Core\UserInterface;
use Roensby\SymfonyDrupalJsonApi\Entity\Helper\DateTimeSupport;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Mapping\ClassMetadata;

abstract class Comment extends ContentEntityBase implements CommentInterface
{
    use DateTimeSupport;

    /**
     * @Groups("attributes")
     * @var ?DateTime
     */
    protected $changed;

    /**
     * @Groups("attributes")
     * @var ?DateTime
     */
    protected $created;

    /**
     * @Groups("attributes")
     * @SerializedName("drupal_internal__cid")
     * @var ?int
     */
    protected $id;

    /**
     * @Groups("relationships")
     * @var ?EntityBase
     */
    protected $entityId;

    /**
     * @Groups("attributes")
     * @var ?string
     */
    protected $entityType = Constant::UNDEFINED;

    /**
     * @Groups("attributes")
     * @var ?string
     */
    protected $fieldName;

    /**
     * @Groups("attributes")
     * @var ?string
     */
    protected $homepage;

    /**
     * @Groups("attributes")
     * @var ?string
     */
    protected $hostname;

    /**
     * @Groups("attributes")
     * @var ?string
     */
    protected $mail;

    /**
     * @Groups("attributes")
     * @var ?string
     */
    protected $name;

    /**
     * @Groups("relationships")
     * @var ?CommentInterface
     */
    protected $pid;

    /**
     * @Groups("attributes")
     * @var ?bool
     */
    protected $status;

    /**
     * @Groups("attributes")
     * @var ?string
     */
    protected $subject;

    /**
     * @Groups("attributes")
     * @var ?string
     */
    protected $thread;

    /**
     * @var string
     */
    protected $type = Constant::ENTITY_COMMENT;

    /**
     * @Groups("relationships")
     * @var ?User
     */
    protected $uid;

    /**
     * Getters.
     */

    public function getChanged(): ?DateTime
    {
        return $this->changed;
    }

    public function getCreated(): ?DateTime
    {
        return $this->created;
    }

    public function getEntityId(): ?EntityBaseInterface
    {
        return $this->entityId;
    }

    public function getFieldName(): ?string
    {
        return $this->fieldName;
    }

    public function getHomepage(): ?string
    {
        return $this->homepage;
    }

    public function getHostname(): ?string
    {
        return $this->hostname;
    }

    public function getMail(): ?string
    {
        return $this->mail;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getPid(): ?CommentInterface
    {
        return $this->pid;
    }

    public function getStatus(): ?bool
    {
        return $this->status;
    }

    public function getSubject(): ?string
    {
        return $this->subject;
    }

    public function getThread(): ?string
    {
        return $this->thread;
    }

    public function getUid(): ?UserInterface
    {
        return $this->uid;
    }

    /**
     * Validation.
     *
     * @param ClassMetadata $metadata
     */
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('entityId', new NotBlank());
        $metadata->addPropertyConstraint('fieldName', new NotBlank());
        parent::loadValidatorMetadata($metadata);
    }

    /**
     * Setters.
     */

    public function setChanged($changed): CommentInterface
    {
        $this->changed = $this->getDateTime($changed);
        return $this;
    }

    public function setCreated($created): CommentInterface
    {
        $this->created = $this->getDateTime($created);
        return $this;
    }

    public function setEntityId(?EntityBaseInterface $entityId): CommentInterface
    {
        $this->entityId = $entityId;
        return $this;
    }

    public function setFieldName(?string $fieldName): CommentInterface
    {
        $this->fieldName = $fieldName;
        return $this;
    }

    public function setHomepage(?string $homepage): CommentInterface
    {
        $this->homepage = $homepage;
        return $this;
    }

    public function setHostname(?string $hostname): CommentInterface
    {
        $this->hostname = $hostname;
        return $this;
    }

    public function setMail(?string $mail): CommentInterface
    {
        $this->mail = $mail;
        return $this;
    }

    public function setName(?string $name): CommentInterface
    {
        $this->name = $name;
        return $this;
    }

    public function setPid(?CommentInterface $pid): CommentInterface
    {
        $this->pid = $pid;
        return $this;
    }

    public function setStatus(?bool $status): CommentInterface
    {
        $this->status = $status;
        return $this;
    }

    public function setSubject(?string $subject): CommentInterface
    {
        $this->subject = $subject;
        return $this;
    }

    public function setThread(?string $thread): CommentInterface
    {
        $this->thread = $thread;
        return $this;
    }

    public function setUid(?UserInterface $uid): CommentInterface
    {
        $this->uid = $uid;
        return $this;
    }
}
