<?php declare(strict_types = 1);

namespace Roensby\SymfonyDrupalJsonApi\Entity\Core\Bundle;

use DateTime;
use Roensby\SymfonyDrupalJsonApi\Constant;
use Roensby\SymfonyDrupalJsonApi\Entity\Base\EditorialContentEntityBase;
use Roensby\SymfonyDrupalJsonApi\Entity\Core\UserInterface;
use Roensby\SymfonyDrupalJsonApi\Entity\Helper\DateTimeSupport;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints\NotBlank;

abstract class Node extends EditorialContentEntityBase implements NodeInterface
{
    use DateTimeSupport;

    /**
     * @Groups("attributes")
     * @var ?DateTime
     */
    protected $changed;

    /**
     * @Groups("attributes")
     * @var ?DateTime
     */
    protected $created;

    /**
     * @Groups("attributes")
     * @SerializedName("drupal_internal__nid")
     * @var ?int
     */
    protected $id;

    /**
     * @Groups("attributes")
     * @var ?bool
     */
    protected $promote;

    /**
     * @Groups("attributes")
     * @SerializedName("drupal_internal__vid")
     * @var ?int
     */
    protected $revision;

    /**
     * @Groups("attributes")
     * @var ?bool
     */
    protected $sticky;

    /**
     * @Groups("attributes")
     * @var ?string
     */
    protected $title;

    /**
     * @Groups("attributes")
     * @var ?User
     */
    protected $uid;

    /**
     * @var string
     */
    protected $type = Constant::ENTITY_NODE;

    public function __toString()
    {
        return (string) $this->title;
    }

    /**
     * Getters.
     */

    public function getChanged(): ?DateTime
    {
        return $this->changed;
    }

    public function getCreated(): ?DateTime
    {
        return $this->created;
    }

    public function getPromote(): ?bool
    {
        return $this->promote;
    }

    public function getSticky(): ?bool
    {
        return $this->sticky;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function getUid(): ?UserInterface
    {
        return $this->uid;
    }

    /**
     * Validation.
     *
     * @param ClassMetadata $metadata
     */
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('title', new NotBlank());
    }

    /**
     * Setters.
     */

    public function setChanged($changed): NodeInterface
    {
        $this->changed = $this->getDateTime($changed);
        return $this;
    }

    public function setCreated($created): NodeInterface
    {
        $this->created = $this->getDateTime($created);
        return $this;
    }

    public function setPromote(?bool $promote): NodeInterface
    {
        $this->promote = $promote;
        return $this;
    }

    public function setSticky(?bool $sticky): NodeInterface
    {
        $this->sticky = $sticky;
        return $this;
    }

    public function setTitle(?string $title): NodeInterface
    {
        $this->title = $title;
        return $this;
    }

    public function setUid(?UserInterface $uid): NodeInterface
    {
        $this->uid = $uid;
        return $this;
    }
}
