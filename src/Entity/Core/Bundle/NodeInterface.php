<?php

namespace Roensby\SymfonyDrupalJsonApi\Entity\Core\Bundle;

use DateTime;
use Roensby\SymfonyDrupalJsonApi\Entity\Base\EditorialContentEntityBaseInterface;
use Roensby\SymfonyDrupalJsonApi\Entity\Core\UserInterface;

interface NodeInterface extends EditorialContentEntityBaseInterface
{
    /**
     * Getters.
     */

    public function getChanged(): ?DateTime;

    public function getCreated(): ?DateTime;

    public function getPromote(): ?bool;

    public function getSticky(): ?bool;

    public function getTitle(): ?string;

    public function getUid(): ?UserInterface;

    /**
     * Setters.
     */

    public function setChanged($changed): NodeInterface;

    public function setCreated($created): NodeInterface;

    public function setPromote(?bool $promote): NodeInterface;

    public function setSticky(?bool$sticky): NodeInterface;

    public function setTitle(?string $title): NodeInterface;

    public function setUid(?UserInterface $uid): NodeInterface;
}
