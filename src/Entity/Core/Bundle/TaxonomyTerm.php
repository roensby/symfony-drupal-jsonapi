<?php declare(strict_types = 1);

namespace Roensby\SymfonyDrupalJsonApi\Entity\Core\Bundle;

use DateTime;
use Roensby\SymfonyDrupalJsonApi\Constant;
use Roensby\SymfonyDrupalJsonApi\Entity\Base\EditorialContentEntityBase;
use Roensby\SymfonyDrupalJsonApi\Entity\Helper\DateTimeSupport;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints\NotBlank;

abstract class TaxonomyTerm extends EditorialContentEntityBase implements TaxonomyTermInterface
{
    use DateTimeSupport;

    /**
     * @Groups("attributes")
     * @var ?DateTime
     */
    protected $changed;

    /**
     * @Groups("attributes")
     * @var array
     */
    protected $description = [];

    /**
     * @Groups("attributes")
     * @SerializedName("drupal_internal__tid")
     * @var ?int
     */
    protected $id;

    /**
     * @Groups("attributes")
     * @var ?string
     */
    protected $name;

    /**
     * @Groups("relationships")
     * @var TaxonomyTerm[]
     */
    protected $parent = [];

    /**
     * @Groups("attributes")
     * @SerializedName("drupal_internal__revision_id")
     * @var ?int
     */
    protected $revision;

    /**
     * @var string
     */
    protected $type = Constant::ENTITY_TERM;

    /**
     * @Groups("attributes")
     * @var ?int
     */
    protected $weight;

    public function __toString()
    {
        return (string) $this->name;
    }

    /**
     * Getters.
     */

    public function getChanged(): ?DateTime
    {
        return $this->changed;
    }

    public function getDescription(): array
    {
        return $this->description;
    }

    public function getDescriptionValue(): ?string
    {
        return isset($this->description['value']) ? $this->description['value'] : null;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getParent(): array
    {
        return $this->parent;
    }

    /**
     * Validation.
     *
     * @param ClassMetadata $metadata
     */
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('name', new NotBlank());
        parent::loadValidatorMetadata($metadata);
    }

    /**
     * Setters.
     */

    public function setChanged($changed): TaxonomyTermInterface
    {
        $this->changed = $this->getDateTime($changed);
        return $this;
    }

    public function setDescription(?array $description): TaxonomyTermInterface
    {
        $this->description = isset($description) ? $description : [];
        return $this;
    }

    public function setDescriptionValue(?string $description): TaxonomyTermInterface
    {
        $this->description['value'] = $description;
        return $this;
    }

    public function setName(?string $name): TaxonomyTermInterface
    {
        $this->name = $name;
        return $this;
    }

    public function setParent(array $parent): TaxonomyTermInterface
    {
        $this->parent = $parent;
        return $this;
    }
}
