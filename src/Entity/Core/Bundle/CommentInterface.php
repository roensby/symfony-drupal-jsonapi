<?php

namespace Roensby\SymfonyDrupalJsonApi\Entity\Core\Bundle;

use DateTime;
use Roensby\SymfonyDrupalJsonApi\Entity\Base\ContentEntityBaseInterface;
use Roensby\SymfonyDrupalJsonApi\Entity\Base\EntityBaseInterface;
use Roensby\SymfonyDrupalJsonApi\Entity\Core\UserInterface;

interface CommentInterface extends ContentEntityBaseInterface
{
    /**
     * Getters.
     */

    public function getChanged(): ?DateTime;

    public function getCreated(): ?DateTime;

    public function getEntityId(): ?EntityBaseInterface;

    public function getFieldName(): ?string;

    public function getHomepage(): ?string;

    public function getHostname(): ?string;

    public function getMail(): ?string;

    public function getName(): ?string;

    public function getPid(): ?CommentInterface;

    public function getStatus(): ?bool;

    public function getSubject(): ?string;

    public function getThread(): ?string;

    public function getUid(): ?UserInterface;

    /**
     * Setters.
     */

    public function setChanged($changed): CommentInterface;

    public function setCreated($created): CommentInterface;

    public function setEntityId(?EntityBaseInterface $entity): CommentInterface;

    public function setFieldName(?string $fieldName): CommentInterface;

    public function setHomepage(?string $homepage): CommentInterface;

    public function setHostname(?string $hostname): CommentInterface;

    public function setMail(?string $mail): CommentInterface;

    public function setName(?string $name): CommentInterface;

    public function setPid(?CommentInterface $comment): CommentInterface;

    public function setStatus(?bool $status): CommentInterface;

    public function setThread(?string $thread): CommentInterface;

    public function setUid(?UserInterface $userId): CommentInterface;
}
