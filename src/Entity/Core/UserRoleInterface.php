<?php

namespace Roensby\SymfonyDrupalJsonApi\Entity\Core;

use Roensby\SymfonyDrupalJsonApi\Entity\Base\ConfigEntityBaseInterface;

interface UserRoleInterface extends ConfigEntityBaseInterface
{
    /**
     * Getters.
     */

    public function getIsAdmin(): ?bool;

    public function getPermissions(): array;

    /**
     * Setters.
     */

    public function setIsAdmin(?bool $isAdmin): UserRoleInterface;

    public function setPermissions(array $permissions): UserRoleInterface;
}
