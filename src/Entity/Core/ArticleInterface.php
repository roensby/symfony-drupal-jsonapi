<?php

namespace Roensby\SymfonyDrupalJsonApi\Entity\Core;

use Roensby\SymfonyDrupalJsonApi\Entity\Core\Bundle\NodeInterface;

interface ArticleInterface extends NodeInterface
{
    /**
     * Getters.
     */

    public function getBody(): array;

    public function getBodyValue(): ?string;

    public function getImage(): ?FileInterface;

    public function getTags(): array;

    /**
     * Setters.
     */

    public function setBody(array $body): ArticleInterface;

    public function setBodyValue(?string $value): ArticleInterface;

    public function setImage(?FileInterface $image): ArticleInterface;

    public function setTags(array $tags): ArticleInterface;
}
