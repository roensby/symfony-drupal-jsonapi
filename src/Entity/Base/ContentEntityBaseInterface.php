<?php

namespace Roensby\SymfonyDrupalJsonApi\Entity\Base;

interface ContentEntityBaseInterface extends EntityBaseInterface
{
    /**
     * Getters.
     */

    public function getId();

    public function getLangcode(): ?string;

    public function getRevision(): ?int;

    public function getUuid(): ?string;

    /**
     * Setters.
     */

    public function setId($id): ContentEntityBaseInterface;

    public function setLangcode(?string $langcode): ContentEntityBaseInterface;

    public function setRevision(?int $revision): ContentEntityBaseInterface;

    public function setUuid(?string $uuid): ContentEntityBaseInterface;
}
