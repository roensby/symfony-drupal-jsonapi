<?php declare(strict_types = 1);

namespace Roensby\SymfonyDrupalJsonApi\Entity\Base;

use DateTime;
use Roensby\SymfonyDrupalJsonApi\Entity\Core\User;
use Roensby\SymfonyDrupalJsonApi\Entity\Core\UserInterface;
use Roensby\SymfonyDrupalJsonApi\Entity\Helper\DateTimeSupport;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Mapping\ClassMetadata;

abstract class EditorialContentEntityBase extends ContentEntityBase implements EditorialContentEntityBaseInterface
{
    use DateTimeSupport;

    /**
     * @Groups("attributes")
     * @var ?DateTime
     */
    protected $revisionCreated;

    /**
     * @Groups("attributes")
     * @var ?string
     */
    protected $revisionLogMessage;

    /**
     * @Groups("relationships")
     * @var ?User
     */
    protected $revisionUser;

    /**
     * @Groups("attributes")
     * @var ?bool
     */
    protected $status;

    /**
     * Getters.
     */

    public function getRevisionCreated(): ?DateTime
    {
        return $this->revisionCreated;
    }

    public function getRevisionLogMessage(): ?string
    {
        return $this->revisionLogMessage;
    }

    public function getRevisionUser(): ?UserInterface
    {
        return $this->revisionUser;
    }

    public function getStatus(): ?bool
    {
        return $this->status;
    }

    /**
     * Validation.
     *
     * @param ClassMetadata $metadata
     */
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('status', new NotNull());
    }

    /**
     * Setters.
     */

    public function setRevisionCreated($revisionCreated): EditorialContentEntityBaseInterface
    {
        $this->revisionCreated = $this->getDateTime($revisionCreated);
        return $this;
    }

    public function setRevisionLogMessage(?string $revisionLogMessage): EditorialContentEntityBaseInterface
    {
        $this->revisionLogMessage = $revisionLogMessage;
        return $this;
    }

    public function setRevisionUser(?UserInterface $user): EditorialContentEntityBaseInterface
    {
        $this->revisionUser = $user;
        return $this;
    }

    public function setStatus(?bool $status): EditorialContentEntityBaseInterface
    {
        $this->status = $status;
        return $this;
    }
}
