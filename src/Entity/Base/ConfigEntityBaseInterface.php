<?php

namespace Roensby\SymfonyDrupalJsonApi\Entity\Base;

interface ConfigEntityBaseInterface extends EntityBaseInterface
{
    /**
     * Getters.
     */

    public function getId(): ?string;

    public function getLabel(): ?string;

    public function getLangcode(): ?string;

    public function getStatus(): ?bool;

    public function getUuid(): ?string;

    /**
     * Setters.
     */

    public function setId(?string $id): ConfigEntityBaseInterface;

    public function setLabel(?string $label): ConfigEntityBaseInterface;

    public function setLangcode(?string $langcode): ConfigEntityBaseInterface;

    public function setStatus(?bool $status): ConfigEntityBaseInterface;

    public function setUuid(?string $uuid): ConfigEntityBaseInterface;
}
