<?php

namespace Roensby\SymfonyDrupalJsonApi\Entity\Base;

use DateTime;
use Roensby\SymfonyDrupalJsonApi\Entity\Core\UserInterface;

interface EditorialContentEntityBaseInterface extends ContentEntityBaseInterface
{
    /**
     * Getters.
     */

    public function getRevisionCreated(): ?DateTime;

    public function getRevisionLogMessage(): ?string;

    public function getRevisionUser(): ?UserInterface;

    public function getStatus(): ?bool;

    /**
     * Setters.
     */

    public function setRevisionCreated($revisionCreated): EditorialContentEntityBaseInterface;

    public function setRevisionLogMessage(?string $revisionLogMessage): EditorialContentEntityBaseInterface;

    public function setRevisionUser(?UserInterface $user): EditorialContentEntityBaseInterface;

    public function setStatus(?bool $status): EditorialContentEntityBaseInterface;
}
