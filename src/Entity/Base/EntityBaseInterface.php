<?php

namespace Roensby\SymfonyDrupalJsonApi\Entity\Base;

interface EntityBaseInterface
{
    /**
     * Getters.
     */

    public function getEntityBundle(): string;

    public function getEntityType(): string;

    public function getUuid(): ?string;
}
