<?php declare(strict_types = 1);

namespace Roensby\SymfonyDrupalJsonApi\Entity\Base;

use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

abstract class ContentEntityBase extends EntityBase implements ContentEntityBaseInterface
{
    /**
     * @Groups("attributes")
     * @SerializedName("drupal_internal__id")
     * @var ?int|string
     */
    protected $id;

    /**
     * @Groups("attributes")
     * @var ?string
     */
    protected $langcode;

    /**
     * @Groups("attributes")
     * @var ?int
     */
    protected $revision;

    /**
     * @Groups("id")
     * @SerializedName("id")
     * @var ?string
     */
    protected $uuid;

    public function __toString()
    {
        return (string) $this->uuid;
    }

    /**
     * Getters.
     */

    public function getId()
    {
        return $this->id;
    }

    public function getLangcode(): ?string
    {
        return $this->langcode;
    }

    public function getRevision(): ?int
    {
        return $this->revision;
    }

    public function getUuid(): ?string
    {
        return $this->uuid;
    }

    /**
     * Setters.
     */

    public function setId($id): ContentEntityBaseInterface
    {
        $this->id = $id;
        return $this;
    }

    public function setLangcode(?string $langcode): ContentEntityBaseInterface
    {
        $this->langcode = $langcode;
        return $this;
    }

    public function setRevision(?int $revision): ContentEntityBaseInterface
    {
        $this->revision = $revision;
        return $this;
    }

    public function setUuid(?string $uuid): ContentEntityBaseInterface
    {
        $this->uuid = $uuid;
        return $this;
    }
}
