<?php declare(strict_types = 1);

namespace Roensby\SymfonyDrupalJsonApi\Entity\Base;

use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Mapping\ClassMetadata;

abstract class ConfigEntityBase extends EntityBase implements ConfigEntityBaseInterface
{
    /**
     * @Groups("attributes")
     * @SerializedName("drupal_internal__id")
     * @var ?string
     */
    protected $id;

    /**
     * @Groups("attributes")
     * @var ?string
     */
    protected $label;

    /**
     * @Groups("attributes")
     * @var ?string
     */
    protected $langcode;

    /**
     * @Groups("attributes")
     * @var ?bool
     */
    protected $status;

    /**
     * @Groups("id")
     * @SerializedName("id")
     * @var ?string
     */
    protected $uuid;

    public function __toString()
    {
        return (string) $this->uuid;
    }

    /**
     * Getters.
     */

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function getLangcode(): ?string
    {
        return $this->langcode;
    }

    public function getStatus(): ?bool
    {
        return $this->status;
    }

    public function getUuid(): ?string
    {
        return $this->uuid;
    }

    /**
     * Setters.
     */

    public function setId(?string $id): ConfigEntityBaseInterface
    {
        $this->id = $id;
        return $this;
    }

    public function setLabel(?string $label): ConfigEntityBaseInterface
    {
        $this->label = $label;
        return $this;
    }

    public function setLangcode(?string $langcode): ConfigEntityBaseInterface
    {
        $this->langcode = $langcode;
        return $this;
    }

    public function setStatus(?bool $status): ConfigEntityBaseInterface
    {
        $this->status = $status;
        return $this;
    }

    public function setUuid(?string $uuid): ConfigEntityBaseInterface
    {
        $this->uuid = $uuid;
        return $this;
    }
}
