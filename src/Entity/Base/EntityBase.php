<?php declare(strict_types = 1);

namespace Roensby\SymfonyDrupalJsonApi\Entity\Base;

use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Mapping\ClassMetadata;

abstract class EntityBase implements EntityBaseInterface
{
    /**
     * @var string
     */
    protected $bundle;

    /**
     * @var string
     */
    protected $type;

    /**
     * Getters.
     */

    public function getEntityBundle(): string
    {
        return $this->bundle;
    }

    public function getEntityType(): string
    {
        return $this->type;
    }

    /**
     * Validation.
     *
     * @param ClassMetadata $metadata
     */
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('bundle', new NotBlank());
        $metadata->addPropertyConstraint('type', new NotBlank());
    }
}
