<?php declare(strict_types = 1);

namespace Roensby\SymfonyDrupalJsonApi;

use Doctrine\Common\Annotations\AnnotationRegistry;
use Roensby\SymfonyDrupalJsonApi\DependencyInjection\SymfonyDrupalJsonapiExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class RoensbySymfonyDrupalJsonapiBundle extends Bundle
{
    public function boot()
    {
        // Make sure that Doctrine can read annotations.
        // See https://stackoverflow.com/a/55038107.
        AnnotationRegistry::registerLoader('class_exists');
    }

    public function getContainerExtension()
    {
        return new SymfonyDrupalJsonapiExtension();
    }
}
