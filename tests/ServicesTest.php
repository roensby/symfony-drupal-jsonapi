<?php

namespace Roensby\SymfonyDrupalJsonApi\Tests;

use Roensby\SymfonyDrupalJsonApi\Factory\EntityFactory;
use Roensby\SymfonyDrupalJsonApi\Security\GuardAuthenticator\LoginAuthenticator;
use Roensby\SymfonyDrupalJsonApi\Security\GuardAuthenticator\OAuthRefreshAuthenticator;
use Roensby\SymfonyDrupalJsonApi\Security\GuardAuthenticator\PresetAuthenticator;
use Roensby\SymfonyDrupalJsonApi\Security\User\UserProvider;
use Roensby\SymfonyDrupalJsonApi\Storage\EntityStorage;
use Roensby\SymfonyDrupalJsonApi\Storage\FileStorage;
use Roensby\SymfonyDrupalJsonApi\Transformer\FileUploadTransformer;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ServicesTest extends KernelTestCase
{
    /**
     * @covers Roensby\SymfonyDrupalJsonApi\DependencyInjection\SymfonyDrupalJsonapiExtension
     */
    public function testServiceWiring()
    {
        $kernel = new TestingKernel('test', true);
        $kernel->boot();
        $container = $kernel->getContainer();
        $this->assertInstanceOf(LoginAuthenticator::class, $container->get('Roensby\SymfonyDrupalJsonApi\Security\GuardAuthenticator\LoginAuthenticator'));
        $this->assertInstanceOf(PresetAuthenticator::class, $container->get('Roensby\SymfonyDrupalJsonApi\Security\GuardAuthenticator\PresetAuthenticator'));
        $this->assertInstanceOf(OAuthRefreshAuthenticator::class, $container->get('Roensby\SymfonyDrupalJsonApi\Security\GuardAuthenticator\OAuthRefreshAuthenticator'));
        $this->assertInstanceOf(UserProvider::class, $container->get('Roensby\SymfonyDrupalJsonApi\Security\User\UserProvider'));
        $this->assertInstanceOf(EntityFactory::class, $container->get('Roensby\SymfonyDrupalJsonApi\Factory\EntityFactory'));
        $this->assertInstanceOf(EntityStorage::class, $container->get('Roensby\SymfonyDrupalJsonApi\Storage\EntityStorage'));
        $this->assertInstanceOf(FileStorage::class, $container->get('Roensby\SymfonyDrupalJsonApi\Storage\FileStorage'));
        $this->assertInstanceOf(FileUploadTransformer::class, $container->get('Roensby\SymfonyDrupalJsonApi\Transformer\FileUploadTransformer'));
    }
}
