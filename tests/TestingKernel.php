<?php

namespace Roensby\SymfonyDrupalJsonApi\Tests;

use Roensby\SymfonyDrupalJsonApi\RoensbySymfonyDrupalJsonapiBundle;
use Roensby\SymfonyDrupalJsonApi\Security\User\OAuthUser;
use Roensby\SymfonyDrupalJsonApi\Security\User\UserProvider;
use Symfony\Bundle\FrameworkBundle\FrameworkBundle;
use Symfony\Bundle\SecurityBundle\SecurityBundle;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Kernel;

class TestingKernel extends Kernel
{
    public function registerBundles()
    {
        return [
            new FrameworkBundle(),
            new SecurityBundle(),
            new RoensbySymfonyDrupalJsonapiBundle(),
        ];
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(function (ContainerBuilder $container) {
            // Configure symfony/framework.
            $container->loadFromExtension('framework', [
                'secret' => 'test',
                'session' => [
                    'enabled' => true,
                    'storage_id' => 'session.storage.mock_file',
                ],
                'router' => [
                    'resource' => '~',
                ],
            ]);
            $container->loadFromExtension('security', [
                'encoders' => [
                    OAuthUser::class => [
                        'algorithm' => 'auto',
                    ],
                ],
                'providers' => [
                    'app_user_provider' => [
                        'id' => UserProvider::class,
                    ],
                ],
                'firewalls' => [
                    'test' => [
                        'pattern' => '^/(_(profiler|wdt)|css|images|js)/',
                        'security' => false,
                    ],
                ],
            ]);
            $container->loadFromExtension('symfony_drupal_jsonapi', [
                'jsonapi' => [
                    'endpoint' => 'http://example/jsonapi',
                    'csrf_endpoint' => 'http://example/session/token',
                ],
                'oauth2' => [
                    'endpoint' => 'http://example/oauth/token',
                    'client_id' => 'lorem',
                    'client_secret' => 'ipsum',
                    'interactive_login' => [
                        'login_route' => 'app_login',
                        'login_redirect' => 'frontpage',
                    ],
                    'preset_user' => [
                        'username' => 'lorem',
                        'password' => 'ipsum',
                    ],
                ],
            ]);
        });
    }
}
