# Symfony Drupal JSONAPI

A Symfony JSONAPI client with built-in support for Drupal entities using OAuth2
for authentication.

## Installation instructions

1. Run `composer require roensby/symfony-drupal-jsonapi`

1. Configure the library:
    ```yaml
    symfony_drupal_jsonapi:
      jsonapi:
        # The Drupal jsonapi endpoint.
        endpoint:             'http://example/jsonapi'
        # The Drupal CSRF endpoint.
        csrf_endpoint:        'http://example/session/token'
      oauth2:
        # The Drupal simple_oauth module endpoint.
        endpoint:             'http://example/oauth/token'
        # A uuid string given by a Drupal consumer entity.
        client_id:            'CLIENT ID'
        # A secret string shared between your Symfony application and Drupal.
        client_secret:        'CLIENT SECRET'
        # Use the interactive login if users log in.
        interactive_login:
          # Where to redirect when authentication fails.
          login_route:        app_login
          # Where to redirect when authentication succeeds.
          login_redirect:     frontpage
        # Use preset user for non-interactive applications.
        preset_user:
          # A Drupal username.
          username:           'USERNAME'
          # The corresponding password.
          password:           'SOME PASSWORD'
    ```

1. Use a combination of either the `LoginAuthenticator` or
`PresetAuthenticator` class with the
`OAuthRefreshAuthenticator` class. 

## Usage instructions

### Load taxonomy term of bundle 'tag' by tid

```php
<?php

namespace App\Controller;

use Roensby\SymfonyDrupalJsonApi\Entity\Core\Tag;
use Roensby\SymfonyDrupalJsonApi\Entity\Core\TagInterface;
use Roensby\SymfonyDrupalJsonApi\Factory\EntityFactory;
use Roensby\SymfonyDrupalJsonApi\JsonApi\Filter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class MyController extends AbstractController {

    /**
     * Renders a tag.
     */
    public function getTag(string $tid): Response
    {
        /** @var EntityFactory $entityFactory */        
        $entityFactory = $this->get(EntityFactory::class);
        /** @var TagInterface $tag */
        $tag = $entityFactory->query(Tag::class)
            ->addFilter(new Filter('drupal_internal__tid', '=', $tid))
            ->load();
        return $this->render('my_template', [
            'description' => $tag->getDescription(),
            'name' => $tag->getName(),
        ]);
    }
}
```

## Security

Setting up the GuardAuthenticator class.

For more information, see
[Custom Authentication System with Guard](https://symfony.com/doc/current/security/guard_authentication.html)
.

```yaml
security:
    encoders:
        Roensby\SymfonyDrupalJsonApi\Security\User\OAuthUser:
            algorithm: auto

    # https://symfony.com/doc/current/security.html#where-do-users-come-from-user-providers
    providers:
        app_user_provider:
            id: Roensby\SymfonyDrupalJsonApi\Security\User\UserProvider
    firewalls:
        main:
          guard:
            authenticators:
              - Roensby\SymfonyDrupalJsonApi\Security\GuardAuthenticator\LoginAuthenticator
              - Roensby\SymfonyDrupalJsonApi\Security\GuardAuthenticator\OAuthRefreshAuthenticator
            entry_point: Roensby\SymfonyDrupalJsonApi\Security\LoginAuthenticator
```

Uses OAuth to authenticate requests.  
For more information, see
[the Simple OAuth module](https://www.drupal.org/project/simple_oauth).
